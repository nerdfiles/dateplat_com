# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PrototypeCustomer'
        db.create_table('dateplat_com_prototypecustomer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 3, 22, 0, 0), blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=60, null=True, blank=True)),
            ('json', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('dateplat_com', ['PrototypeCustomer'])

        # Adding model 'Brand'
        db.create_table('dateplat_com_brand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')(max_length=255, db_column='Name')),
        ))
        db.send_create_signal('dateplat_com', ['Brand'])

        # Adding model 'Product'
        db.create_table('dateplat_com_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')(max_length=100, db_column='Name')),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0, db_column='Price', decimal_places=2, max_digits=12)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Brand'], db_column='BrandID')),
        ))
        db.send_create_signal('dateplat_com', ['Product'])

        # Adding model 'User'
        db.create_table('dateplat_com_user', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(default='', max_length=75, db_column='Email')),
        ))
        db.send_create_signal('dateplat_com', ['User'])

        # Adding model 'Answer'
        db.create_table('dateplat_com_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Text', blank=True)),
        ))
        db.send_create_signal('dateplat_com', ['Answer'])

        # Adding model 'Question'
        db.create_table('questions.question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')(db_column='Position')),
            ('title', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Title', blank=True)),
            ('image', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Image', blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, db_column='Text', blank=True)),
        ))
        db.send_create_signal('dateplat_com', ['Question'])

        # Adding M2M table for field answers on 'Question'
        db.create_table('questions.question_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm['questions.question'], null=False)),
            ('answer', models.ForeignKey(orm['dateplat_com.answer'], null=False))
        ))
        db.create_unique('questions.question_answers', ['question_id', 'answer_id'])

        # Adding model 'Pantry'
        db.create_table('dateplat_com_pantry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.User'], db_column='UserID')),
            ('num_bathrooms', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='NumBathrooms', blank=True)),
            ('age_range', self.gf('django.db.models.fields.TextField')(null=True, db_column='AgeRange', blank=True)),
            ('hand_wash_num', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='HandWashNum', blank=True)),
            ('soap_type', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='SoapType', blank=True)),
            ('like_soap_refills', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='LikeSoapRefills', blank=True)),
            ('soap_brand', self.gf('django.db.models.fields.related.ForeignKey')(related_name='soap_brand', null=True, db_column='SoapBrand', to=orm['dateplat_com.Brand'])),
            ('paper_brand', self.gf('django.db.models.fields.related.ForeignKey')(related_name='paper_brand', null=True, db_column='PaperBrand', to=orm['dateplat_com.Brand'])),
        ))
        db.send_create_signal('dateplat_com', ['Pantry'])

        # Adding model 'PantryProduct'
        db.create_table('dateplat_com_pantryproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=0, db_column='Quantity')),
            ('pantries', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Pantry'], db_column='PantryID')),
            ('products', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Product'], db_column='ProductID')),
        ))
        db.send_create_signal('dateplat_com', ['PantryProduct'])


    def backwards(self, orm):
        # Deleting model 'PrototypeCustomer'
        db.delete_table('dateplat_com_prototypecustomer')

        # Deleting model 'Brand'
        db.delete_table('dateplat_com_brand')

        # Deleting model 'Product'
        db.delete_table('dateplat_com_product')

        # Deleting model 'User'
        db.delete_table('dateplat_com_user')

        # Deleting model 'Answer'
        db.delete_table('dateplat_com_answer')

        # Deleting model 'Question'
        db.delete_table('questions.question')

        # Removing M2M table for field answers on 'Question'
        db.delete_table('questions.question_answers')

        # Deleting model 'Pantry'
        db.delete_table('dateplat_com_pantry')

        # Deleting model 'PantryProduct'
        db.delete_table('dateplat_com_pantryproduct')


    models = {
        'dateplat_com.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'db_column': "'Text'", 'blank': 'True'})
        },
        'dateplat_com.brand': {
            'Meta': {'object_name': 'Brand'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '255', 'db_column': "'Name'"})
        },
        'dateplat_com.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'age_range': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'AgeRange'", 'blank': 'True'}),
            'hand_wash_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'HandWashNum'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'like_soap_refills': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'LikeSoapRefills'", 'blank': 'True'}),
            'num_bathrooms': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'NumBathrooms'", 'blank': 'True'}),
            'paper_brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paper_brand'", 'null': 'True', 'db_column': "'PaperBrand'", 'to': "orm['dateplat_com.Brand']"}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['dateplat_com.Product']", 'through': "orm['dateplat_com.PantryProduct']", 'symmetrical': 'False'}),
            'soap_brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'soap_brand'", 'null': 'True', 'db_column': "'SoapBrand'", 'to': "orm['dateplat_com.Brand']"}),
            'soap_type': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'db_column': "'SoapType'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dateplat_com.User']", 'db_column': "'UserID'"})
        },
        'dateplat_com.pantryproduct': {
            'Meta': {'object_name': 'PantryProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pantries': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dateplat_com.Pantry']", 'db_column': "'PantryID'"}),
            'products': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dateplat_com.Product']", 'db_column': "'ProductID'"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_column': "'Quantity'"})
        },
        'dateplat_com.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['dateplat_com.Brand']", 'db_column': "'BrandID'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '100', 'db_column': "'Name'"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'db_column': "'Price'", 'decimal_places': '2', 'max_digits': '12'})
        },
        'dateplat_com.prototypecustomer': {
            'Meta': {'object_name': 'PrototypeCustomer'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 3, 22, 0, 0)', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        'questions.question': {
            'Meta': {'object_name': 'Question'},
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['dateplat_com.Answer']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'db_column': "'Image'", 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'db_column': "'Position'"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'db_column': "'Text'", 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'db_column': "'Title'", 'blank': 'True'})
        },
        'dateplat_com.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '75', 'db_column': "'Email'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['dateplat_com']
