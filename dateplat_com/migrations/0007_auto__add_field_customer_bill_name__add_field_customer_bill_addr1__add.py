# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Customer.bill_name'
        db.add_column(u'dateplat_com_customer', 'bill_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.bill_addr1'
        db.add_column(u'dateplat_com_customer', 'bill_addr1',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.bill_addr2'
        db.add_column(u'dateplat_com_customer', 'bill_addr2',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.bill_city'
        db.add_column(u'dateplat_com_customer', 'bill_city',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.bill_state'
        db.add_column(u'dateplat_com_customer', 'bill_state',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=3, blank=True),
                      keep_default=False)

        # Adding field 'Customer.bill_zip'
        db.add_column(u'dateplat_com_customer', 'bill_zip',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=15, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_different'
        db.add_column(u'dateplat_com_customer', 'ship_different',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Customer.ship_name'
        db.add_column(u'dateplat_com_customer', 'ship_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_addr1'
        db.add_column(u'dateplat_com_customer', 'ship_addr1',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_addr2'
        db.add_column(u'dateplat_com_customer', 'ship_addr2',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_city'
        db.add_column(u'dateplat_com_customer', 'ship_city',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_state'
        db.add_column(u'dateplat_com_customer', 'ship_state',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=3, blank=True),
                      keep_default=False)

        # Adding field 'Customer.ship_zip'
        db.add_column(u'dateplat_com_customer', 'ship_zip',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=15, blank=True),
                      keep_default=False)

        # Adding field 'Customer.accepted_terms'
        db.add_column(u'dateplat_com_customer', 'accepted_terms',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Customer.stripe_token'
        db.add_column(u'dateplat_com_customer', 'stripe_token',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Customer.bill_name'
        db.delete_column(u'dateplat_com_customer', 'bill_name')

        # Deleting field 'Customer.bill_addr1'
        db.delete_column(u'dateplat_com_customer', 'bill_addr1')

        # Deleting field 'Customer.bill_addr2'
        db.delete_column(u'dateplat_com_customer', 'bill_addr2')

        # Deleting field 'Customer.bill_city'
        db.delete_column(u'dateplat_com_customer', 'bill_city')

        # Deleting field 'Customer.bill_state'
        db.delete_column(u'dateplat_com_customer', 'bill_state')

        # Deleting field 'Customer.bill_zip'
        db.delete_column(u'dateplat_com_customer', 'bill_zip')

        # Deleting field 'Customer.ship_different'
        db.delete_column(u'dateplat_com_customer', 'ship_different')

        # Deleting field 'Customer.ship_name'
        db.delete_column(u'dateplat_com_customer', 'ship_name')

        # Deleting field 'Customer.ship_addr1'
        db.delete_column(u'dateplat_com_customer', 'ship_addr1')

        # Deleting field 'Customer.ship_addr2'
        db.delete_column(u'dateplat_com_customer', 'ship_addr2')

        # Deleting field 'Customer.ship_city'
        db.delete_column(u'dateplat_com_customer', 'ship_city')

        # Deleting field 'Customer.ship_state'
        db.delete_column(u'dateplat_com_customer', 'ship_state')

        # Deleting field 'Customer.ship_zip'
        db.delete_column(u'dateplat_com_customer', 'ship_zip')

        # Deleting field 'Customer.accepted_terms'
        db.delete_column(u'dateplat_com_customer', 'accepted_terms')

        # Deleting field 'Customer.stripe_token'
        db.delete_column(u'dateplat_com_customer', 'stripe_token')


    models = {
        u'product.brand': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Brand'},
            'canonical_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'origin': ('django.db.models.fields.TextField', [], {}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'brands'", 'to': u"orm['product.BrandType']"})
        },
        u'product.brandtype': {
            'Meta': {'object_name': 'BrandType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.preference': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Preference'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'preferences'", 'to': u"orm['product.Brand']"}),
            'has_many': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.preferencevalue': {
            'Meta': {'object_name': 'PreferenceValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'preference': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'options'", 'to': u"orm['product.Preference']"})
        },
        u'product.product': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['product.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'max_per_shipment': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent_product': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['product.Product']", 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '12', 'decimal_places': '2'}),
            'unit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'}),
            'volume': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        u'dateplat_com.customer': {
            'Meta': {'object_name': 'Customer'},
            'accepted_terms': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'added_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'bill_addr1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'bill_addr2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'bill_city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'bill_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'bill_state': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'bill_zip': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ship_addr1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ship_addr2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ship_city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ship_different': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ship_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ship_state': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'ship_zip': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'stripe_token': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'})
        },
        u'dateplat_com.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['questions.Answer']", 'symmetrical': 'False'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['dateplat_com.Customer']", 'unique': 'True'}),
            'excluded_products': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'excluded_product'", 'blank': 'True', 'to': u"orm['product.Product']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preferences': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.PreferenceValue']", 'symmetrical': 'False'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.Product']", 'symmetrical': 'False'})
        },
        u'questions.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {'default': '1', 'blank': 'True'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['dateplat_com']
