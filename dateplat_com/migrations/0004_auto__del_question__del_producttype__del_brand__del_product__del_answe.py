# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Question'
        db.delete_table(u'questions.question')

        # Deleting model 'ProductType'
        db.delete_table(u'dateplat_com_producttype')

        # Deleting model 'Brand'
        db.delete_table(u'dateplat_com_brand')

        # Deleting model 'Product'
        db.delete_table(u'dateplat_com_product')

        # Deleting model 'Answer'
        db.delete_table(u'dateplat_com_answer')

        # Removing M2M table for field answers on 'Customer'
        db.delete_table('dateplat_com_customer_answers')

        # Adding M2M table for field answers on 'Pantry'
        db.create_table(u'dateplat_com_pantry_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('pantry', models.ForeignKey(orm[u'dateplat_com.pantry'], null=False)),
            ('answer', models.ForeignKey(orm[u'questions.answer'], null=False))
        ))
        db.create_unique(u'dateplat_com_pantry_answers', ['pantry_id', 'answer_id'])


    def backwards(self, orm):
        # Adding model 'Question'
        db.create_table(u'questions.question', (
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['Question'])

        # Adding model 'ProductType'
        db.create_table(u'dateplat_com_producttype', (
            ('type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['ProductType'])

        # Adding model 'Brand'
        db.create_table(u'dateplat_com_brand', (
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'dateplat_com', ['Brand'])

        # Adding model 'Product'
        db.create_table(u'dateplat_com_product', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=12, decimal_places=2)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.ProductType'])),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Brand'])),
        ))
        db.send_create_signal(u'dateplat_com', ['Product'])

        # Adding model 'Answer'
        db.create_table(u'dateplat_com_answer', (
            ('text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, blank=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(related_name='answers', to=orm['questions.question'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['Answer'])

        # Adding M2M table for field answers on 'Customer'
        db.create_table(u'dateplat_com_customer_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('customer', models.ForeignKey(orm[u'dateplat_com.customer'], null=False)),
            ('answer', models.ForeignKey(orm[u'dateplat_com.answer'], null=False))
        ))
        db.create_unique(u'dateplat_com_customer_answers', ['customer_id', 'answer_id'])

        # Removing M2M table for field answers on 'Pantry'
        db.delete_table('dateplat_com_pantry_answers')


    models = {
        u'product.brand': {
            'Meta': {'object_name': 'Brand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '12', 'decimal_places': '2'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.ProductType']"})
        },
        u'product.producttype': {
            'Meta': {'object_name': 'ProductType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'dateplat_com.customer': {
            'Meta': {'object_name': 'Customer'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'dateplat_com.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['questions.Answer']", 'symmetrical': 'False'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['dateplat_com.Customer']", 'unique': 'True'}),
            'excluded_products': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'excluded_product'", 'symmetrical': 'False', 'to': u"orm['product.Product']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.Product']", 'symmetrical': 'False'})
        },
        u'dateplat_com.preferences': {
            'Meta': {'object_name': 'Preferences'},
            'age_range': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['dateplat_com.Customer']", 'unique': 'True'}),
            'hand_wash_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'like_soap_refills': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'soap_type': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'questions.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {'default': '1', 'blank': 'True'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['dateplat_com']
