# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'dateplat_com_user')

        # Deleting model 'PrototypeCustomer'
        db.delete_table(u'dateplat_com_prototypecustomer')

        # Adding model 'Customer'
        db.create_table(u'dateplat_com_customer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('added_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['Customer'])

        # Adding M2M table for field answers on 'Customer'
        db.create_table(u'dateplat_com_customer_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('customer', models.ForeignKey(orm[u'dateplat_com.customer'], null=False)),
            ('answer', models.ForeignKey(orm[u'dateplat_com.answer'], null=False))
        ))
        db.create_unique(u'dateplat_com_customer_answers', ['customer_id', 'answer_id'])

        # Adding model 'ProductType'
        db.create_table(u'dateplat_com_producttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'dateplat_com', ['ProductType'])

        # Adding model 'Preferences'
        db.create_table(u'dateplat_com_preferences', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['dateplat_com.Customer'], unique=True)),
            ('age_range', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('hand_wash_num', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('soap_type', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('like_soap_refills', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['Preferences'])

        # Deleting field 'Brand.type'
        db.delete_column(u'dateplat_com_brand', 'type')

        # Adding field 'Brand.slug'
        db.add_column(u'dateplat_com_brand', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=255),
                      keep_default=False)


        # Changing field 'Brand.name'
        db.alter_column(u'dateplat_com_brand', 'name', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Question.image'
        db.alter_column(u'questions.question', 'image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True))

        # Changing field 'Question.title'
        db.alter_column(u'questions.question', 'title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))
        # Adding field 'Product.type'
        db.add_column(u'dateplat_com_product', 'type',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['dateplat_com.ProductType']),
                      keep_default=False)


        # Changing field 'Product.name'
        db.alter_column(u'dateplat_com_product', 'name', self.gf('django.db.models.fields.CharField')(max_length=100))
        # Deleting field 'Pantry.age_range'
        db.delete_column(u'dateplat_com_pantry', 'age_range')

        # Deleting field 'Pantry.user'
        db.delete_column(u'dateplat_com_pantry', 'user_id')

        # Deleting field 'Pantry.soap_brand'
        db.delete_column(u'dateplat_com_pantry', 'soap_brand_id')

        # Deleting field 'Pantry.paper_brand'
        db.delete_column(u'dateplat_com_pantry', 'paper_brand_id')

        # Deleting field 'Pantry.soap_type'
        db.delete_column(u'dateplat_com_pantry', 'soap_type')

        # Deleting field 'Pantry.created_date'
        db.delete_column(u'dateplat_com_pantry', 'created_date')

        # Deleting field 'Pantry.hand_wash_num'
        db.delete_column(u'dateplat_com_pantry', 'hand_wash_num')

        # Deleting field 'Pantry.like_soap_refills'
        db.delete_column(u'dateplat_com_pantry', 'like_soap_refills')

        # Adding field 'Pantry.customer'
        db.add_column(u'dateplat_com_pantry', 'customer',
                      self.gf('django.db.models.fields.related.OneToOneField')(default=0, to=orm['dateplat_com.Customer'], unique=True),
                      keep_default=False)

        # Removing M2M table for field answers on 'Pantry'
        db.delete_table('dateplat_com_pantry_answers')


    def backwards(self, orm):
        # Adding model 'User'
        db.create_table(u'dateplat_com_user', (
            ('email', self.gf('django.db.models.fields.EmailField')(default='', max_length=75)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['User'])

        # Adding model 'PrototypeCustomer'
        db.create_table(u'dateplat_com_prototypecustomer', (
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('json', self.gf('django.db.models.fields.TextField')()),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=60, null=True, blank=True)),
        ))
        db.send_create_signal(u'dateplat_com', ['PrototypeCustomer'])

        # Deleting model 'Customer'
        db.delete_table(u'dateplat_com_customer')

        # Removing M2M table for field answers on 'Customer'
        db.delete_table('dateplat_com_customer_answers')

        # Deleting model 'ProductType'
        db.delete_table(u'dateplat_com_producttype')

        # Deleting model 'Preferences'
        db.delete_table(u'dateplat_com_preferences')

        # Adding field 'Brand.type'
        db.add_column(u'dateplat_com_brand', 'type',
                      self.gf('django.db.models.fields.TextField')(default='', max_length=255),
                      keep_default=False)

        # Deleting field 'Brand.slug'
        db.delete_column(u'dateplat_com_brand', 'slug')


        # Changing field 'Brand.name'
        db.alter_column(u'dateplat_com_brand', 'name', self.gf('django.db.models.fields.TextField')(max_length=255))

        # Changing field 'Question.image'
        db.alter_column(u'questions.question', 'image', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Changing field 'Question.title'
        db.alter_column(u'questions.question', 'title', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))
        # Deleting field 'Product.type'
        db.delete_column(u'dateplat_com_product', 'type_id')


        # Changing field 'Product.name'
        db.alter_column(u'dateplat_com_product', 'name', self.gf('django.db.models.fields.TextField')(max_length=100))
        # Adding field 'Pantry.age_range'
        db.add_column(u'dateplat_com_pantry', 'age_range',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pantry.user'
        db.add_column(u'dateplat_com_pantry', 'user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['dateplat_com.User']),
                      keep_default=False)

        # Adding field 'Pantry.soap_brand'
        db.add_column(u'dateplat_com_pantry', 'soap_brand',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='soap_brand', null=True, to=orm['dateplat_com.Brand']),
                      keep_default=False)

        # Adding field 'Pantry.paper_brand'
        db.add_column(u'dateplat_com_pantry', 'paper_brand',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='paper_brand', null=True, to=orm['dateplat_com.Brand']),
                      keep_default=False)

        # Adding field 'Pantry.soap_type'
        db.add_column(u'dateplat_com_pantry', 'soap_type',
                      self.gf('django.db.models.fields.TextField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pantry.created_date'
        db.add_column(u'dateplat_com_pantry', 'created_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default='', blank=True),
                      keep_default=False)

        # Adding field 'Pantry.hand_wash_num'
        db.add_column(u'dateplat_com_pantry', 'hand_wash_num',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pantry.like_soap_refills'
        db.add_column(u'dateplat_com_pantry', 'like_soap_refills',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Pantry.customer'
        db.delete_column(u'dateplat_com_pantry', 'customer_id')

        # Adding M2M table for field answers on 'Pantry'
        db.create_table(u'dateplat_com_pantry_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('pantry', models.ForeignKey(orm[u'dateplat_com.pantry'], null=False)),
            ('answer', models.ForeignKey(orm[u'dateplat_com.answer'], null=False))
        ))
        db.create_unique(u'dateplat_com_pantry_answers', ['pantry_id', 'answer_id'])


    models = {
        u'dateplat_com.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.question']"}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'dateplat_com.brand': {
            'Meta': {'object_name': 'Brand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        },
        u'dateplat_com.customer': {
            'Meta': {'object_name': 'Customer'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['dateplat_com.Answer']", 'null': 'True', 'symmetrical': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'dateplat_com.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['dateplat_com.Customer']", 'unique': 'True'}),
            'excluded_products': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'excluded_product'", 'symmetrical': 'False', 'to': u"orm['dateplat_com.Product']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['dateplat_com.Product']", 'symmetrical': 'False'})
        },
        u'dateplat_com.preferences': {
            'Meta': {'object_name': 'Preferences'},
            'age_range': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['dateplat_com.Customer']", 'unique': 'True'}),
            'hand_wash_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'like_soap_refills': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'soap_type': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'dateplat_com.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dateplat_com.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '12', 'decimal_places': '2'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dateplat_com.ProductType']"})
        },
        u'dateplat_com.producttype': {
            'Meta': {'object_name': 'ProductType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'questions.question': {
            'Meta': {'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['dateplat_com']
