# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'PantryProduct'
        db.delete_table(u'dateplat_com_pantryproduct')


        # Renaming column for 'User.email' to match new field type.
        db.rename_column(u'dateplat_com_user', 'Email', 'email')
        # Changing field 'User.email'
        db.alter_column(u'dateplat_com_user', 'email', self.gf('django.db.models.fields.EmailField')(max_length=75))
        # Adding field 'Brand.type'
        db.add_column(u'dateplat_com_brand', 'type',
                      self.gf('django.db.models.fields.TextField')(default=0, max_length=255),
                      keep_default=False)


        # Renaming column for 'Brand.name' to match new field type.
        db.rename_column(u'dateplat_com_brand', 'Name', 'name')
        # Changing field 'Brand.name'
        db.alter_column(u'dateplat_com_brand', 'name', self.gf('django.db.models.fields.TextField')(max_length=255))
        # Removing M2M table for field answers on 'Question'
        db.delete_table('questions.question_answers')


        # Renaming column for 'Question.position' to match new field type.
        db.rename_column(u'questions.question', 'Position', 'position')
        # Changing field 'Question.position'
        db.alter_column(u'questions.question', 'position', self.gf('django.db.models.fields.IntegerField')())

        # Renaming column for 'Question.image' to match new field type.
        db.rename_column(u'questions.question', 'Image', 'image')
        # Changing field 'Question.image'
        db.alter_column(u'questions.question', 'image', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Renaming column for 'Question.title' to match new field type.
        db.rename_column(u'questions.question', 'Title', 'title')
        # Changing field 'Question.title'
        db.alter_column(u'questions.question', 'title', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Renaming column for 'Question.text' to match new field type.
        db.rename_column(u'questions.question', 'Text', 'text')
        # Changing field 'Question.text'
        db.alter_column(u'questions.question', 'text', self.gf('django.db.models.fields.TextField')(null=True))

        # Renaming column for 'Product.price' to match new field type.
        db.rename_column(u'dateplat_com_product', 'Price', 'price')
        # Changing field 'Product.price'
        db.alter_column(u'dateplat_com_product', 'price', self.gf('django.db.models.fields.DecimalField')(max_digits=12, decimal_places=2))

        # Renaming column for 'Product.brand' to match new field type.
        db.rename_column(u'dateplat_com_product', 'BrandID', 'brand_id')
        # Changing field 'Product.brand'
        db.alter_column(u'dateplat_com_product', 'brand_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Brand']))

        # Renaming column for 'Product.name' to match new field type.
        db.rename_column(u'dateplat_com_product', 'Name', 'name')
        # Changing field 'Product.name'
        db.alter_column(u'dateplat_com_product', 'name', self.gf('django.db.models.fields.TextField')(max_length=100))
        # Adding field 'Answer.question'
        db.add_column(u'dateplat_com_answer', 'question',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='answers', to=orm['questions.question']),
                      keep_default=False)


        # Renaming column for 'Answer.text' to match new field type.
        db.rename_column(u'dateplat_com_answer', 'Text', 'text')
        # Changing field 'Answer.text'
        db.alter_column(u'dateplat_com_answer', 'text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))
        # Deleting field 'Pantry.num_bathrooms'
        db.delete_column(u'dateplat_com_pantry', 'NumBathrooms')

        # Adding field 'Pantry.created_date'
        db.add_column(u'dateplat_com_pantry', 'created_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 3, 26, 0, 0), blank=True),
                      keep_default=False)

        # Adding M2M table for field excluded_products on 'Pantry'
        db.create_table(u'dateplat_com_pantry_excluded_products', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('pantry', models.ForeignKey(orm[u'dateplat_com.pantry'], null=False)),
            ('product', models.ForeignKey(orm[u'dateplat_com.product'], null=False))
        ))
        db.create_unique(u'dateplat_com_pantry_excluded_products', ['pantry_id', 'product_id'])

        # Adding M2M table for field answers on 'Pantry'
        db.create_table(u'dateplat_com_pantry_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('pantry', models.ForeignKey(orm[u'dateplat_com.pantry'], null=False)),
            ('answer', models.ForeignKey(orm[u'dateplat_com.answer'], null=False))
        ))
        db.create_unique(u'dateplat_com_pantry_answers', ['pantry_id', 'answer_id'])


        # Renaming column for 'Pantry.soap_brand' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'SoapBrand', 'soap_brand_id')
        # Changing field 'Pantry.soap_brand'
        db.alter_column(u'dateplat_com_pantry', 'soap_brand_id', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['dateplat_com.Brand']))

        # Renaming column for 'Pantry.age_range' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'AgeRange', 'age_range')
        # Changing field 'Pantry.age_range'
        db.alter_column(u'dateplat_com_pantry', 'age_range', self.gf('django.db.models.fields.TextField')(null=True))

        # Renaming column for 'Pantry.paper_brand' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'PaperBrand', 'paper_brand_id')
        # Changing field 'Pantry.paper_brand'
        db.alter_column(u'dateplat_com_pantry', 'paper_brand_id', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['dateplat_com.Brand']))

        # Renaming column for 'Pantry.soap_type' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'SoapType', 'soap_type')
        # Changing field 'Pantry.soap_type'
        db.alter_column(u'dateplat_com_pantry', 'soap_type', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Renaming column for 'Pantry.hand_wash_num' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'HandWashNum', 'hand_wash_num')
        # Changing field 'Pantry.hand_wash_num'
        db.alter_column(u'dateplat_com_pantry', 'hand_wash_num', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Renaming column for 'Pantry.like_soap_refills' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'LikeSoapRefills', 'like_soap_refills')
        # Changing field 'Pantry.like_soap_refills'
        db.alter_column(u'dateplat_com_pantry', 'like_soap_refills', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Renaming column for 'Pantry.user' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'UserID', 'user_id')
        # Changing field 'Pantry.user'
        db.alter_column(u'dateplat_com_pantry', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.User']))
        # Adding M2M table for field products on 'Pantry'
        db.create_table(u'dateplat_com_pantry_products', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('pantry', models.ForeignKey(orm[u'dateplat_com.pantry'], null=False)),
            ('product', models.ForeignKey(orm[u'dateplat_com.product'], null=False))
        ))
        db.create_unique(u'dateplat_com_pantry_products', ['pantry_id', 'product_id'])


        # Changing field 'PrototypeCustomer.pub_date'
        db.alter_column(u'dateplat_com_prototypecustomer', 'pub_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

    def backwards(self, orm):
        # Adding model 'PantryProduct'
        db.create_table(u'dateplat_com_pantryproduct', (
            ('products', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Product'], db_column='ProductID')),
            ('pantries', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Pantry'], db_column='PantryID')),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=0, db_column='Quantity')),
        ))
        db.send_create_signal('dateplat_com', ['PantryProduct'])


        # Renaming column for 'User.email' to match new field type.
        db.rename_column(u'dateplat_com_user', 'email', 'Email')
        # Changing field 'User.email'
        db.alter_column(u'dateplat_com_user', 'Email', self.gf('django.db.models.fields.EmailField')(max_length=75, db_column='Email'))
        # Deleting field 'Brand.type'
        db.delete_column(u'dateplat_com_brand', 'type')


        # Renaming column for 'Brand.name' to match new field type.
        db.rename_column(u'dateplat_com_brand', 'name', 'Name')
        # Changing field 'Brand.name'
        db.alter_column(u'dateplat_com_brand', 'Name', self.gf('django.db.models.fields.TextField')(max_length=255, db_column='Name'))
        # Adding M2M table for field answers on 'Question'
        db.create_table(u'questions.question_answers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm['questions.question'], null=False)),
            ('answer', models.ForeignKey(orm['dateplat_com.answer'], null=False))
        ))
        db.create_unique(u'questions.question_answers', ['question_id', 'answer_id'])


        # Renaming column for 'Question.position' to match new field type.
        db.rename_column(u'questions.question', 'position', 'Position')
        # Changing field 'Question.position'
        db.alter_column(u'questions.question', 'Position', self.gf('django.db.models.fields.IntegerField')(db_column='Position'))

        # Renaming column for 'Question.image' to match new field type.
        db.rename_column(u'questions.question', 'image', 'Image')
        # Changing field 'Question.image'
        db.alter_column(u'questions.question', 'Image', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Image'))

        # Renaming column for 'Question.title' to match new field type.
        db.rename_column(u'questions.question', 'title', 'Title')
        # Changing field 'Question.title'
        db.alter_column(u'questions.question', 'Title', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Title'))

        # Renaming column for 'Question.text' to match new field type.
        db.rename_column(u'questions.question', 'text', 'Text')
        # Changing field 'Question.text'
        db.alter_column(u'questions.question', 'Text', self.gf('django.db.models.fields.TextField')(null=True, db_column='Text'))

        # Renaming column for 'Product.price' to match new field type.
        db.rename_column(u'dateplat_com_product', 'price', 'Price')
        # Changing field 'Product.price'
        db.alter_column(u'dateplat_com_product', 'Price', self.gf('django.db.models.fields.DecimalField')(decimal_places=2, db_column='Price', max_digits=12))

        # Renaming column for 'Product.brand' to match new field type.
        db.rename_column(u'dateplat_com_product', 'brand_id', 'BrandID')
        # Changing field 'Product.brand'
        db.alter_column(u'dateplat_com_product', 'BrandID', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.Brand'], db_column='BrandID'))

        # Renaming column for 'Product.name' to match new field type.
        db.rename_column(u'dateplat_com_product', 'name', 'Name')
        # Changing field 'Product.name'
        db.alter_column(u'dateplat_com_product', 'Name', self.gf('django.db.models.fields.TextField')(max_length=100, db_column='Name'))
        # Deleting field 'Answer.question'
        db.delete_column(u'dateplat_com_answer', 'question_id')


        # Renaming column for 'Answer.text' to match new field type.
        db.rename_column(u'dateplat_com_answer', 'text', 'Text')
        # Changing field 'Answer.text'
        db.alter_column(u'dateplat_com_answer', 'Text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='Text'))
        # Adding field 'Pantry.num_bathrooms'
        db.add_column(u'dateplat_com_pantry', 'num_bathrooms',
                      self.gf('django.db.models.fields.IntegerField')(null=True, db_column='NumBathrooms', blank=True),
                      keep_default=False)

        # Deleting field 'Pantry.created_date'
        db.delete_column(u'dateplat_com_pantry', 'created_date')

        # Removing M2M table for field excluded_products on 'Pantry'
        db.delete_table('dateplat_com_pantry_excluded_products')

        # Removing M2M table for field answers on 'Pantry'
        db.delete_table('dateplat_com_pantry_answers')


        # Renaming column for 'Pantry.soap_brand' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'soap_brand_id', 'SoapBrand')
        # Changing field 'Pantry.soap_brand'
        db.alter_column(u'dateplat_com_pantry', 'SoapBrand', self.gf('django.db.models.fields.related.ForeignKey')(null=True, db_column='SoapBrand', to=orm['dateplat_com.Brand']))

        # Renaming column for 'Pantry.age_range' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'age_range', 'AgeRange')
        # Changing field 'Pantry.age_range'
        db.alter_column(u'dateplat_com_pantry', 'AgeRange', self.gf('django.db.models.fields.TextField')(null=True, db_column='AgeRange'))

        # Renaming column for 'Pantry.paper_brand' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'paper_brand_id', 'PaperBrand')
        # Changing field 'Pantry.paper_brand'
        db.alter_column(u'dateplat_com_pantry', 'PaperBrand', self.gf('django.db.models.fields.related.ForeignKey')(null=True, db_column='PaperBrand', to=orm['dateplat_com.Brand']))

        # Renaming column for 'Pantry.soap_type' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'soap_type', 'SoapType')
        # Changing field 'Pantry.soap_type'
        db.alter_column(u'dateplat_com_pantry', 'SoapType', self.gf('django.db.models.fields.TextField')(max_length=255, null=True, db_column='SoapType'))

        # Renaming column for 'Pantry.hand_wash_num' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'hand_wash_num', 'HandWashNum')
        # Changing field 'Pantry.hand_wash_num'
        db.alter_column(u'dateplat_com_pantry', 'HandWashNum', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='HandWashNum'))

        # Renaming column for 'Pantry.like_soap_refills' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'like_soap_refills', 'LikeSoapRefills')
        # Changing field 'Pantry.like_soap_refills'
        db.alter_column(u'dateplat_com_pantry', 'LikeSoapRefills', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='LikeSoapRefills'))

        # Renaming column for 'Pantry.user' to match new field type.
        db.rename_column(u'dateplat_com_pantry', 'user_id', 'UserID')
        # Changing field 'Pantry.user'
        db.alter_column(u'dateplat_com_pantry', 'UserID', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dateplat_com.User'], db_column='UserID'))
        # Removing M2M table for field products on 'Pantry'
        db.delete_table('dateplat_com_pantry_products')


        # Changing field 'PrototypeCustomer.pub_date'
        db.alter_column(u'dateplat_com_prototypecustomer', 'pub_date', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        u'dateplat_com.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.question']"}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'dateplat_com.brand': {
            'Meta': {'object_name': 'Brand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.TextField', [], {'max_length': '255'})
        },
        u'dateplat_com.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'age_range': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['dateplat_com.Answer']", 'symmetrical': 'False'}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'excluded_products': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'excluded_product'", 'symmetrical': 'False', 'to': u"orm['dateplat_com.Product']"}),
            'hand_wash_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'like_soap_refills': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'paper_brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paper_brand'", 'null': 'True', 'to': u"orm['dateplat_com.Brand']"}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['dateplat_com.Product']", 'symmetrical': 'False'}),
            'soap_brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'soap_brand'", 'null': 'True', 'to': u"orm['dateplat_com.Brand']"}),
            'soap_type': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dateplat_com.User']"})
        },
        u'dateplat_com.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dateplat_com.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '12', 'decimal_places': '2'})
        },
        u'dateplat_com.prototypecustomer': {
            'Meta': {'object_name': 'PrototypeCustomer'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'questions.question': {
            'Meta': {'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'dateplat_com.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['dateplat_com']
