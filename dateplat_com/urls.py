# -*- coding: utf-8 -*- #

from django.conf import settings
from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from product.views import BrandTypeList, CategoryList
from dateplat_com.views import CustomerDetailUpdate, CustomerCreate
from questions.views import QuestionList
from schedule.views import ShipmentCharge

# assets management
from django.conf.urls.defaults import *

# admin essentials
from django.contrib import admin
from schedule.views import ShipmentItemUpdate

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'dateplat_com.views.home', name='home'),
                       url(r'^login$', 'dateplat_com.views.login', name='login'),
                       url(r'^secret$', 'dateplat_com.views.secret', name='secret'),
                       url(r'^sign-up$', 'dateplat_com.views.sign_up', name='sign-up'),
                       url(r'^dashboard/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^dashboard/', include(admin.site.urls)),
)

urlpatterns += patterns('dateplat_com.views',
                        url(r'^api/brandtypes/$', BrandTypeList.as_view(), name='brand-list'),
                        url(r'^api/categories/$', CategoryList.as_view(), name='product-list'),
                        url(r'^api/questions/$', QuestionList.as_view(), name='question-list'),
                        url(r'^api/customer/create/$', CustomerCreate.as_view(), name='customer-create'),
                        url(r'^api/customer/(?P<email>[\w\._@-]+)/$', CustomerDetailUpdate.as_view(), name='customer-detail'),
                        url(r'^api/shipmentitem/update/(?P<pk>\d+)/$', ShipmentItemUpdate.as_view(), name='shipment-item-update'),
                        url(r'^api/shipment/charge/$', ShipmentCharge.as_view(), name='shipment-charge')
)

# Format suffixes
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

urlpatterns += patterns('',
                        url(r'^static(?P<path>.*)$', 'django.views.static.serve',
                            {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
)

urlpatterns += patterns('',
                        url(r'^docs(?P<path>.*)$', 'django.views.static.serve',
                            {'document_root': settings.DOC_ROOT, 'show_indexes': True}),
)

handler404 = 'dateplat_com.views.error_404'
handler500 = 'dateplat_com.views.error_500'
