from django.shortcuts import render_to_response
from django.template import RequestContext
from rest_framework import generics
from django.conf import settings
from models import Customer
from serializers import CustomerSerializer
###Helpers

def render_response(request, *args, **kwargs):
    kwargs['context_instance'] = RequestContext(request)
    return render_to_response(*args, **kwargs)


#Static content

def home(request):
    context = {'STRIPE_PUBLIC_KEY': settings.STRIPE_PUBLIC_KEY}
    return render_response(request, 'index.tmpl.haml', context)


def login(request):
    return render_response(request, 'index.tmpl.haml')


def secret(request):
    return render_response(request, 'index.tmpl.haml')


def sign_up(request):
    return render_response(request, 'index.tmpl.haml')


def error_404(request):
    return render_response(request, '404.tmpl.haml')


def error_500(request):
    return render_response(request, '500.tmpl.haml')


class CustomerDetailUpdate(generics.RetrieveUpdateAPIView):
    slug_field =  slug_url_kwarg = 'email'
    model = Customer
    serializer_class = CustomerSerializer


class CustomerCreate(generics.CreateAPIView):
    model = Customer
    serializer_class = CustomerSerializer
