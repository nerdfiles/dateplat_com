from rest_framework import serializers
from models import Customer, Pantry
from schedule.serializer import ShipmentSerializer


class PantrySerializer(serializers.ModelSerializer):
    answers = serializers.PrimaryKeyRelatedField(many=True)
    preferences = serializers.PrimaryKeyRelatedField(many=True)
    products = serializers.PrimaryKeyRelatedField(many=True)
    excluded_products = serializers.PrimaryKeyRelatedField(many=True)
    shipments = ShipmentSerializer(many=True)

    class Meta:
        model = Pantry


class CustomerSerializer(serializers.ModelSerializer):
    pantry = PantrySerializer()

    def validate_email(self, attrs, source):
        value = attrs[source]
        if Customer.objects.filter(email=value.lower()).exists():
            raise serializers.ValidationError("Customer with this email already exists")
        return attrs

    def save(self):
        super(CustomerSerializer, self).save()

        pantry_data = self.init_data.get('pantry', None)
        if pantry_data:
            pantry_data.update({'customer':self.object.pk})
            ps = PantrySerializer(data=pantry_data)
            if ps.is_valid():
                ps.save()
                ps.object.create_shipments()
            else:
                raise Exception(ps.errors)

    class Meta:
        model = Customer