from django.contrib import admin
from dateplat_com.models import Customer, Pantry, Address


class PantryAdmin(admin.ModelAdmin):
    list_display = ('customer',)
    filter_horizontal = ('products', 'excluded_products', 'answers', 'preferences')


class CustomerAdmin(admin.ModelAdmin):
    pass

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Pantry, PantryAdmin)
admin.site.register(Address)
