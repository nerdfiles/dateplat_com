# -*- coding: utf-8 -*- #
import datetime
from annoying.decorators import signals
from annoying.functions import get_object_or_None
from django.db import models
from mailing.models import Template, TEMPLATE_KEYS
from product.models import Product, PreferenceValue, Coefficient, PREFERENCE_KEYS
from questions.models import Answer

# utils

_ = lambda s: s

import logging

l = logging.getLogger('django.db.backends')
l.setLevel(logging.DEBUG)
l.addHandler(logging.StreamHandler())


def add_month(date, month=1):
    new_month = (((date.month - 1) + month) % 12) + 1
    new_year = date.year + (((date.month - 1) + month) // 12)
    return datetime.date(new_year, new_month, date.day)


class Address(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    addr1 = models.CharField(max_length=128, blank=True, null=True)
    addr2 = models.CharField(max_length=128, blank=True, null=True)
    city = models.CharField(max_length=128, blank=True, null=True)
    state = models.CharField(max_length=3, blank=True, null=True)
    zip = models.CharField(max_length=15, blank=True, null=True)


class Customer(models.Model):
    email = models.EmailField(unique=True)
    added_at = models.DateTimeField(auto_now_add=True)

    bill_address = models.ForeignKey(
        Address,
        blank=True,
        null=True,
        related_name='bill_customer_set')
    ship_different = models.BooleanField()
    ship_address = models.ForeignKey(
        Address,
        blank=True,
        null=True,
        related_name='ship_customer_set')

    accepted_terms = models.DateTimeField(blank=True, null=True)
    stripe_token = models.CharField(max_length=128, blank=True)

    def __unicode__(self):
        return unicode(self.email)

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        return super(Customer, self).save(*args, **kwargs)

    def send_signup_email(self):
        template = get_object_or_None(Template, key=TEMPLATE_KEYS[0][0])
        if template:
            data = {str(self.email): 1}
            msg = template.create_message(
                to=self.email,
                data=data
            )
            msg.send()


class Pantry(models.Model):
    customer = models.OneToOneField(Customer, unique=True)
    answers = models.ManyToManyField(Answer, )
    preferences = models.ManyToManyField(PreferenceValue, )
    products = models.ManyToManyField(
        Product,
        )  # The only products, that will be used for schedule really.
    excluded_products = models.ManyToManyField(
        Product,
        blank=True,
        related_name='excluded_product')  # Not required, but used for researches, and statistics(may be)

    _need_refills = None
    _need_formats = None
    _answers_dict = None
    _coefficient_dict = None
    _product_need = None
    _product_max_qty = None

    @property
    def need_refills(self):
        if self._need_refills is None:
            self._need_refills = self.preferences.filter(
                preference__key=PREFERENCE_KEYS[0][0],
                value=1).exists()
        return self._need_refills

    @property
    def need_formats(self):
        if self._need_formats is None:
            self._need_formats = self.preferences.filter(
                preference__key=PREFERENCE_KEYS[1][0]).exists()
        return self._need_formats

    @property
    def answers_dict(self):
        if not self._answers_dict:
            self._answers_dict = {
                answer['question__key']: answer
                ['value']
                for answer in self.answers.values('question__key', 'value')}
        return self._answers_dict

    @property
    def coefficient_dict(self):
        if not self._coefficient_dict:
            self._coefficient_dict = {
                c['question__key']: c
                ['coefficient']
                for c in Coefficient.objects.filter(
                    product__in=self.products.all()).values('question__key',
                                                            'coefficient')}
        return self._coefficient_dict

    @property
    def product_need(self):
        if not self._product_need:
            self._product_need = {
                product.pk: product.get_month_need
                (self.answers_dict, self.coefficient_dict)
                for product in self.products.all()}
        return self._product_need

    @property
    def product_max_qty(self):
        if not self._product_max_qty:
            self._product_max_qty = {
                product['pk']: product
                ['max_per_shipment']
                for product in self.products.values('pk', 'max_per_shipment')}
        return self._product_max_qty

    def get_first_shipment(self):
        return self.shipments.latest('-date')

    def get_last_shipment(self):
        try:
            return self.shipments.latest('date')
        except self.shipments.model.DoesNotExist:
            return None

    def _create_shipment_item(
            self,
            product,
            last_shipment,
            new_shipment,
            month_num):
        shipment_items = []
        # product that is used instead of original (refill, gel or foamy soap)
        format = product
        # Format handling
        if self.need_formats and product.formats:
            format_pref_value = self.preferences.get(
                preference__key=PREFERENCE_KEYS[1][0])
            format = product.formats.get(
                format_rule__preference_value=format_pref_value)
        # Refill handling
        if self.need_refills and format.refill:
            refill = format.refill
            if month_num % refill.refill_rule.send_every == 1:
                # 50% of month 1 quantity (remember that 1st month needs only
                # 2/3 of requirement)
                p_need = self.product_need[product.pk] / 3
                product_item = new_shipment.create_shipment_item(format, p_need)
                r_need = self.product_need[product.pk]\
                    - product_item.quantity * format.volume\
                    - last_shipment.product_balance_dict.get(product.pk, 0)

                shipment_item = new_shipment.create_shipment_item(
                    refill,
                    r_need)
                shipment_items.append(product_item)
            else:
                p_month_need = self.product_need[product.pk]
                balance = last_shipment.product_balance_dict.get(product.pk, 0)
                shipment_item = new_shipment.create_shipment_item(
                    refill,
                    p_month_need -
                    balance)
        else:

            product_balance = last_shipment.product_balance_dict[
                product.pk]    # Product quantity that user has after last month
            # Product quantity that user needs per month (estimated)
            product_month_need = self.product_need[product.pk]
            # Product quantity that user needs by current moment
            product_need = product_month_need - product_balance
            shipment_item = new_shipment.create_shipment_item(
                product=product,
                need=product_need)
        shipment_items.append(shipment_item)
        return shipment_items

    def create_first_shipment(self):
        from schedule.models import Shipment, ShipmentItem

        if not self.shipments.exists():
            date_shipment = datetime.date.today().replace(day=15)
            first_shipment = Shipment(pantry=self, date=date_shipment)
            first_shipment.save()
            shipment_items = []

            for product in self.products.select_related('brand').all():
                product_month_need = self.product_need[
                    product.pk]    # Product quantity that user needs per month (estimated)
                product_need = 2. / 3 * product_month_need
                shipment_item = first_shipment.create_shipment_item(
                    product=product,
                    need=product_need)
                shipment_items.append(shipment_item)

            ShipmentItem.objects.bulk_create(shipment_items)
            return first_shipment

    def create_next_shipment(self):
        from schedule.models import Shipment, ShipmentItem

        last_shipment = self.get_last_shipment()
        if not last_shipment:
            return self.create_first_shipment()

        new_shipment = Shipment(pantry=self, date=add_month(last_shipment.date))
        new_shipment.save()

        month_num = self.shipments.count()

        shipment_items = []

        for product in self.products.select_related('brand'):
            shipment_item = self._create_shipment_item(
                product,
                last_shipment,
                new_shipment,
                month_num)
            shipment_items += shipment_item

        ShipmentItem.objects.bulk_create(shipment_items)
        l.debug('-----------------create_next_shipment executed-------------')
        return new_shipment

    def create_shipments(self):
        for i in range(24):
            self.create_next_shipment()

    def __unicode__(self):
        return unicode(self.customer)


@signals.post_save(sender=Customer)
def notify(instance, created, **kwargs):
    if created:
        instance.send_signup_email()
