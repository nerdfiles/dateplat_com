#[dateplat.com](http://dateplat.com) | [repo]() [license](LICENSE) |

Dateplat brings Evaluative Modeling to timestamped metadata structures notarized by off-chain distributed plats (journaling chains). 

##Installation for Developers

[``pythonbrew``](https://github.com/utahta/pythonbrew) is recommended for management of Python installations.
Essentially, ``pythonbrew`` serves as a manager of package managers, like [``pip``](http://pypi.python.org/pypi/pip)
and [``setuptools``](http://pypi.python.org/pypi/setuptools) (``easy_install``).

###Dev setup

Follow along!

    $ pythonbrew switch 2.7.2                               # Assuming Python 2.7.2; do $ pip install virtualenv -U to get the updated versioqn
    $ virtualenv --distribute {env_name}                    # I'm using "pypantry" for my environemnt name
    $ git clone -b dev git@bitbucket.org:nerdfiles/dateplat_com.git
    $ source {env_name}/bin/activate
    $ cd dateplat_com/
    $ pip install -r requirements.txt                       # To install the requirements list into {env_name}, under "$HOME/.virtualenvs" or whever you have set virtualenv
    $ python manage.py syncdb --all --settings=settings.{ENV}
    $ python manage.py runserver --settings=settings.{ENV}  # Using Werkzeug debugging server instead of "runserver"

The app expects postgres running locally on port 5432, with a database named dateplat_io, a user named postgres, and no password. You can do this by installing postgres, then running:

    $ psql -h localhost                                     # Connect to the local db (default: postgres/admin)
    # CREATE DATABASE dateplat_io
    # CREATE USER postgres SUPERUSER

To re-load the virtual environment after completing some developemnt:

    $ workon pypantry # or whatever name you have given to your virtualenv

You may need to collect static resources.

    $ python manage.py collectstatic --settings=settings.{ENV}

You might also want to set up Coffeescript to watch changes and recompile the JS:

    # run this in _themes\eP\assets\js
    > coffee -o build/ -cw source/app.coffee

###Success!

Huzzah!
