# -*- coding: utf-8 -*- #
from annoying.functions import get_object_or_None
from django.db import models
from questions.models import Question, QUESTION_KEYS
import math

_ = lambda s: s

PREFERENCE_KEYS = (
    ('refills', 'Refills'), ('gel_foamy', 'Gel vs. Foamy'),
    ('scents', 'Scents'))
PRODUCT_UNITS = (
    (0, 'Oz'), (1, 'Load'), (2, 'Bottle'), (3, 'Wipe'), (4, 'Candle'),
    (5, 'Bar'), (6, 'Sheets'), (7, 'Rolls'), (8, 'Boxes'))   # Add more if need


class BrandType(models.Model):
    type = models.CharField(max_length=255)

    def __unicode__(self):
        return unicode(self.type)


class Brand(models.Model):
    full_name = models.CharField(max_length=255,)
    name = models.CharField(max_length=255,)
    slug = models.SlugField(max_length=255,)
    type = models.ForeignKey(BrandType, related_name='brands',)
    logo_image = models.ImageField(upload_to='images',)
    canonical_image = models.ImageField(upload_to='images',)
    origin = models.TextField()
    position = models.IntegerField(default=0)

    class Meta:
        ordering = ('position',)

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Brand, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.name)


class Preference(models.Model):
    name = models.CharField(max_length=255,)
    question = models.CharField(max_length=255,)
    key = models.CharField(choices=PREFERENCE_KEYS, max_length=32)
    has_many = models.BooleanField(default=False,)
    brand = models.ForeignKey(Brand, related_name='preferences',)
    position = models.IntegerField(default=0)

    class Meta:
        ordering = ('position',)
        unique_together = ('key', 'brand')

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Preference, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s - %s' % (self.brand, self.name)


class PreferenceValue(models.Model):
    label = models.CharField(max_length=64,)
    image = models.CharField(max_length=64,)
    value = models.SmallIntegerField(default=1)
    preference = models.ForeignKey(Preference, related_name='options')

    def __unicode__(self):
        return '%s - %s' % (self.preference, self.label)


class Category(models.Model):
    name = models.CharField(max_length=255,)

    def __unicode__(self):
        return unicode(self.name)


class Product(models.Model):
    name = models.CharField(max_length=100,)
    price = models.DecimalField(
        decimal_places=2,
        max_digits=12,
        default=0.00,
        help_text=_('Price for 1 container'))
    brand = models.ForeignKey(Brand,)
    category = models.ForeignKey(Category, related_name='products')
    volume = models.FloatField(default=0.0, help_text=_('Units per container'))
    unit = models.PositiveSmallIntegerField(choices=PRODUCT_UNITS, blank=True)
    # Maximum quantity that can be shipped in one shipment, see AI excel column
    max_per_shipment = models.PositiveIntegerField(
        default=0,
        help_text=_('Maximum quantity per one shipment'))
    image = models.CharField(max_length=255,)
    position = models.IntegerField(default=0)
    parent_product = models.ForeignKey(
        'self',
        related_name='main_product',
        default=None,
        null=True,
        blank=True,
    )

    _coef_dict = None

    @property
    def refill(self):
        try:
            return self.main_product.filter(refill_rule__isnull=False).get()
        except self.DoesNotExist:
            return None

    def is_refill(self):
        return hasattr(self, 'refill_rule')
    is_refill.boolean = True

    @property
    def formats(self):
        return self.main_product.filter(format_rule__isnull=False)

    def is_format(self):
        return self.format_rule.exists()
    is_format.boolean = True

    @property
    def coef_dict(self):
        if not self._coef_dict:
            values = Coefficient.objects.filter(
                product=self).values(
                'question__key',
                'coefficient')
            self._coef_dict = {c['question__key']: c['coefficient']
                               for c in values}
        return self._coef_dict

    class Meta:
        ordering = ('position',)

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Product, self).save(*args, **kwargs)

    # apply 1st answer
    def _get_qty_by_people_cnt(self, answers_dict, coefficient_dict):
        value = answers_dict[QUESTION_KEYS[0][0]]
        obj = get_object_or_None(
            ProductPerPerson,
            product=self,
            num_persons=value)
        if obj:
            return obj.units

    # apply 2nd answer
    def _get_qty_by_babies(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[1][0]
        answer_value = answers_dict[key]

        if answer_value:
            return coefficient_dict[key]

    # apply 3rd answer
    def _get_qty_by_cook_much(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[2][0]
        answer_value = answers_dict[key]
        coefficient = coefficient_dict[key]

        if coefficient:
            return coefficient * (answer_value * .575)

    # apply 4th answer
    def _get_qty_by_age(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[3][0]
        answer_value = answers_dict[key]
        coefficient = coefficient_dict[key]

        if coefficient:
            return coefficient * (.2 + answer_value * .45)

    # apply 5th answer
    def _get_qty_by_wash_hands(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[4][0]
        answer_value = answers_dict[key]
        coefficient = coefficient_dict[key]

        if coefficient:
            return coefficient * (1 - (2 - answer_value) * .25)

    # apply 6th answer
    def _get_qty_by_clean_home(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[5][0]
        answer_value = answers_dict[key]
        coefficient = coefficient_dict[key]

        if coefficient:
            return coefficient * (1 - (2 - answer_value) * .25)

    # apply 7th answer
    def _get_qty_by_bathrooms(self, answers_dict, coefficient_dict):
        key = QUESTION_KEYS[6][0]
        answer_value = answers_dict[key]
        coefficient = coefficient_dict[key]

        if coefficient:
            return max(4, answer_value + 1)

    def get_month_need(self, answers_dict, coefficient_dict):
        result = self._get_qty_by_people_cnt(answers_dict, coefficient_dict)
        result += self._get_qty_by_babies(answers_dict, coefficient_dict) or 0
        result += self._get_qty_by_cook_much(answers_dict,
                                             coefficient_dict) or 0
        result += self._get_qty_by_age(answers_dict, coefficient_dict) or 0
        result += self._get_qty_by_wash_hands(answers_dict,
                                              coefficient_dict) or 0
        result += self._get_qty_by_clean_home(answers_dict,
                                              coefficient_dict) or 0
        result += self._get_qty_by_bathrooms(answers_dict,
                                             coefficient_dict) or 0
        return result

    def get_total(self, product_need):
        if product_need > 0:
            containers = int(math.ceil(product_need / self.volume))
            return self.max_per_shipment if containers >= self.max_per_shipment else containers
        return 0

    def __unicode__(self):
        return '%s - %s' % (self.brand, self.name)


class Coefficient(models.Model):
    product = models.ForeignKey(Product,)
    question = models.ForeignKey(Question,)
    coefficient = models.FloatField()

    class Meta:
        unique_together = ('product', 'question')

# The class will store data from F-O excel columns


class ProductPerPerson(models.Model):
    product = models.ForeignKey(Product,)
    num_persons = models.PositiveSmallIntegerField(default=1)
    units = models.FloatField(default=0)

    class Meta:
        unique_together = ('product', 'num_persons',)


class RefillRule(models.Model):
    refill = models.OneToOneField(
        Product,
        related_name='refill_rule',
        limit_choices_to={
            'parent_product__isnull': False})
    send_every = models.PositiveSmallIntegerField(default=6)


class FormatRule(models.Model):
    product = models.ForeignKey(
        Product,
        related_name='format_rule',
        limit_choices_to={
            'parent_product__isnull': False})
    preference_value = models.ForeignKey(
        PreferenceValue,
        limit_choices_to={
            'preference__key__exact': PREFERENCE_KEYS[1][0]})
