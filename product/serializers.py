from rest_framework import serializers
from product.models import Brand, Product, PreferenceValue, Preference, BrandType, Category


class PreferenceValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreferenceValue
        exclude = ('preference',)


class PreferenceSerializer(serializers.ModelSerializer):
    options = PreferenceValueSerializer(many=True)

    class Meta:
        model = Preference
        exclude = ('brand',)


class BrandSerializer(serializers.ModelSerializer):
    preferences = PreferenceSerializer(many=True)

    class Meta:
        model = Brand
        exclude = ('type', )


class BrandTypeSerializer(serializers.ModelSerializer):
    brands = BrandSerializer(many=True)

    class Meta:
        model = BrandType


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        exclude = ('category',)


class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Category
