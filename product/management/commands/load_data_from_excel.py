# -*- coding: utf-8 -*-
from annoying.functions import get_object_or_None

from django.core.management.base import BaseCommand
from xlrd import open_workbook
from product.models import Brand, Product, ProductPerPerson, Coefficient, Category
from questions.models import Question

BRAND_COL = 0
PRODUCT_COL = 1
PRODUCT_PER_PERSON_START = 4
PRODUCT_PER_PERSON_END = 14
PRODUCT_CATEGORY_COL = 20
PRODUCT_UNITS_COL = 22
PRODUCT_MAX_PER_SHIPMENT_COL = 23
PRODUCT_VOLUME_COL = 24
PRODUCT_PRICE_COL = 26



PEOPLE_COUNT_ROW = 1

UNITS = {
    'Oz' : 0,
    'Load' : 1,
    'Bottle' : 2,
    'Wipes' : 3,
    'Candle' : 4,
    'Bar' : 5,
    'Sheets' : 6,
    'Rolls' : 7,
    'Boxes' : 8
}

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        wb = open_workbook('initial_data v02.xlsx')
        sheet = wb.sheets()[0]
        brand=None
        for row in range(sheet.nrows):
            #Check if brand
            if sheet.cell(row,BRAND_COL).value:
                print 'Brand: %s' % sheet.cell(row,BRAND_COL).value
                brand  = Brand.objects.get(name=sheet.cell(row,BRAND_COL).value)
                continue
                #Check if product
            if sheet.cell(row, PRODUCT_COL).value:
                p_name = sheet.cell(row, PRODUCT_COL).value
                p_price = sheet.cell(row, PRODUCT_PRICE_COL).value
                p_category = get_object_or_None(Category, name=sheet.cell(row, PRODUCT_CATEGORY_COL).value)
                p_volume = sheet.cell(row, PRODUCT_VOLUME_COL).value
                p_unit = UNITS[sheet.cell(row, PRODUCT_UNITS_COL).value]
                p_max_ship = sheet.cell(row, PRODUCT_MAX_PER_SHIPMENT_COL).value

                product = Product(
                    name=p_name,
                    price=p_price,
                    brand=brand,
                    category=p_category,
                    volume=p_volume,
                    unit=p_unit,
                    max_per_shipment=p_max_ship,
                    image=''
                )
                product.save()
                self.stdout.write('Product saved')
                product_per_persons = []
                for i in range(PRODUCT_PER_PERSON_START, PRODUCT_PER_PERSON_END):
                    people_cnt = sheet.cell(PEOPLE_COUNT_ROW, i).value
                    need = sheet.cell(row, i).value
                    pp = ProductPerPerson(
                        product=product,
                        num_persons=people_cnt,
                        units=need
                    )
                    product_per_persons.append(pp)
                ProductPerPerson.objects.bulk_create(product_per_persons)
                self.stdout.write('\tProductPerPersons saved')

                coefficients = []
                for q in Question.objects.all():
                    coef = Coefficient(
                        product=product,
                        question=q,
                        coefficient=1.
                    )
                    coefficients.append(coef)
                Coefficient.objects.bulk_create(coefficients)
                self.stdout.write('\tCoefficients saved')