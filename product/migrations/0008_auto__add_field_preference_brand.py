# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Preference.brand'
        db.add_column(u'product_preference', 'brand',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['product.Brand']),
                      keep_default=False)

        # Removing M2M table for field preferences on 'Brand'
        db.delete_table('product_brand_preferences')


    def backwards(self, orm):
        # Deleting field 'Preference.brand'
        db.delete_column(u'product_preference', 'brand_id')

        # Adding M2M table for field preferences on 'Brand'
        db.create_table(u'product_brand_preferences', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('brand', models.ForeignKey(orm[u'product.brand'], null=False)),
            ('preference', models.ForeignKey(orm[u'product.preference'], null=False))
        ))
        db.create_unique(u'product_brand_preferences', ['brand_id', 'preference_id'])


    models = {
        u'product.brand': {
            'Meta': {'object_name': 'Brand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'brands'", 'to': u"orm['product.BrandType']"})
        },
        u'product.brandtype': {
            'Meta': {'object_name': 'BrandType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.coefficient': {
            'Meta': {'object_name': 'Coefficient'},
            'coefficient': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questions.Question']"})
        },
        u'product.preference': {
            'Meta': {'object_name': 'Preference'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            'has_many': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'product.preferencevalue': {
            'Meta': {'object_name': 'PreferenceValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'preference': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'options'", 'to': u"orm['product.Preference']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            'category': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_per_shipment': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '12', 'decimal_places': '2'}),
            'unit': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'product.productpermonth': {
            'Meta': {'object_name': 'ProductPerMonth'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'units': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['product']