# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Preference', fields ['brand', 'key']
        db.create_unique(u'product_preference', ['brand_id', 'key'])


    def backwards(self, orm):
        # Removing unique constraint on 'Preference', fields ['brand', 'key']
        db.delete_unique(u'product_preference', ['brand_id', 'key'])


    models = {
        u'product.brand': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Brand'},
            'canonical_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'origin': ('django.db.models.fields.TextField', [], {}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'brands'", 'to': u"orm['product.BrandType']"})
        },
        u'product.brandtype': {
            'Meta': {'object_name': 'BrandType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.coefficient': {
            'Meta': {'unique_together': "(('product', 'question'),)", 'object_name': 'Coefficient'},
            'coefficient': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questions.Question']"})
        },
        u'product.preference': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('key', 'brand'),)", 'object_name': 'Preference'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'preferences'", 'to': u"orm['product.Brand']"}),
            'has_many': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'product.preferencevalue': {
            'Meta': {'object_name': 'PreferenceValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'preference': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'options'", 'to': u"orm['product.Preference']"})
        },
        u'product.product': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['product.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'max_per_shipment': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent_product': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['product.Product']", 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '12', 'decimal_places': '2'}),
            'unit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'})
        },
        u'product.productperperson': {
            'Meta': {'unique_together': "(('product', 'num_persons'),)", 'object_name': 'ProductPerPerson'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_persons': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'units': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['product']