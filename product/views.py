from rest_framework import generics
from product.models import Product, BrandType, Category
from product.serializers import ProductSerializer, BrandTypeSerializer, CategorySerializer


class BrandTypeList(generics.ListAPIView):
    model = BrandType
    serializer_class = BrandTypeSerializer


class ProductList(generics.ListAPIView):
    model = Product
    serializer_class = ProductSerializer
    filter_fields = ('brand', 'category')


class CategoryList(generics.ListAPIView):
    model = Category
    serializer_class = CategorySerializer

