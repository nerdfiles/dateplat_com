from django.contrib import admin
from product.models import Brand, Product, ProductPerPerson, Coefficient, PreferenceValue, Preference, BrandType, Category, RefillRule, FormatRule


class PreferenceValueInline(admin.StackedInline):
    model = PreferenceValue
    extra = 0


class PreferenceAdmin(admin.ModelAdmin):
    list_display = ('name', 'key', 'brand', 'has_many', 'position',)
    list_filter = ('key', 'brand',)
    list_editable = ['key', 'position',]
    inlines = [PreferenceValueInline,]

    class Media:
        js = (
            'vendor/jquery/1.9.0/jquery.min.js',
            'vendor/jquery-ui/jquery-ui.min.js',
            'admin/js/admin-list-reorder.js',
        )


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


class BrandAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'slug', 'type', 'logo_image', 'position',)
    prepopulated_fields = {"slug": ("name",)}
    list_editable = ['position',]

    class Media:
        js = (
            'vendor/jquery/1.9.0/jquery.min.js',
            'vendor/jquery-ui/jquery-ui.min.js',
            'admin/js/admin-list-reorder.js',
        )


class CoefficientAdmin(admin.ModelAdmin):
    list_display = ('product', 'question', 'coefficient',)
    list_filter = ('question', 'product',)
    list_editable = ['coefficient',]


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'category', 'volume', 'max_per_shipment', 'price', 'unit', 'is_refill', 'is_format', 'position')
    list_editable = ['volume', 'max_per_shipment', 'price', 'position',]
    list_filter = ('brand', 'category',)

    class Media:
        js = (
            'vendor/jquery/1.9.0/jquery.min.js',
            'vendor/jquery-ui/jquery-ui.min.js',
            'admin/js/admin-list-reorder.js',
        )


class ProductPerPersonAdmin(admin.ModelAdmin):
    list_display = ('product', 'num_persons', 'units',)
    list_editable = ('units',)
    list_filter = ('product__brand', 'num_persons', 'product',)


class RefillRuleAdmin(admin.ModelAdmin):
    list_display = ('refill', 'send_every',)

admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(BrandType)
admin.site.register(Coefficient, CoefficientAdmin)
admin.site.register(ProductPerPerson, ProductPerPersonAdmin)
admin.site.register(Preference, PreferenceAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(RefillRule, RefillRuleAdmin)
admin.site.register(FormatRule)