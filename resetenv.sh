#!/bin/bash
psql -h localhost << EOF
DROP DATABASE beatnote_io;
CREATE DATABASE beatnote_io;
EOF
python manage.py syncdb --settings=settings.local
python manage.py migrate --settings=settings.local
python manage.py loaddata all_data.json --settings=settings.local
