module.exports = () ->

  # Pantry View

  class eP.App.Views.PantryView extends Backbone.View

    close: () ->

      @$el.empty()

      @undelegateEvents()

    initialize: () ->

      @reveal_defaults =
        closeOnBackgroundClick: false
        animation: 'fade'

      $('.m--progress').fadeOut().remove()
      
      # @note check request headers and do not load include app_intro$
      
      app_intro$ = $('.app-intro').detach()

      nice_work_modal$ = $ '#nice-work'

      nice_work_modal$.trigger('reveal:close')

      @email = @options.customer_email

      # if sign up customer is present, you have "working_customer.at(0)" (Collections.Customers)
      @sign_up_customer = @options.sign_up_customer

      $('html,body').addClass 'eP-dashboard'

      $('.app--page').removeClass 'landing'

      @render()

      if @sign_up_customer

        @customers = @sign_up_customer

        @customers.bind('change', @update_dashboard, @)

        @customers = @customers.save()

      else

        loaded_customer = new eP.App.Models.Customer { id: @email }

        loaded_customer.bind('change', @update_dashboard, @)
       
        # GET for customer data
        @customers = loaded_customer.fetch()

        @customers.done (customer) =>

          @customers = new eP.App.Collections.Customers
          
          @customers.reset [customer]

    update_dashboard: (customer) ->

      @working_customer = customer

      @$el.empty()

      @render()

      pantry_calendar$ = @$el.find('.m.m--pantry-calendar')

      $('.m--pantry-calendar .loader').animate({

        opacity: 0

      }, 250, () ->

        el$ = $ @

        el$.slideUp()

        loaded_pantry = customer.get 'pantry'

        # sort months by date
        @loaded_shipments = _.sortBy loaded_pantry.shipments, (shipment) ->
          shipment.date

        first_date = @loaded_shipments[0].date

        creation_date = moment(first_date)

        month_list = []

        month_panel$ = ''

        # filter the first year
        prepared_shipments = _.reject @loaded_shipments, (shipment, i) ->
          if i >= 12
            shipment

        _.each prepared_shipments, (shipment) =>

          shipment_id = shipment.id
          shipment.day = moment(shipment.date).format('Do')
          shipment.month = moment(shipment.date).format('MMM')
          shipment.year = moment(shipment.date).format('YYYY')
          shipment.full_date = moment(shipment.date).format('dddd, MMMM Do YYYY')
          shipment.timeago = moment(shipment.date).endOf('day').fromNow()
          shipments = new eP.App.Collections.Shipments shipment.items
          shipment.products_list = ''
          p = []
          
          p.push '<ul class="m-body" data-shipment="'+shipment_id+'">'

          # per month
          shipments.each (item) ->

            image = if ( item.get('image') && item.get('image') != 'no image') then item.get('image') else '/static/img/demo/meyers-test-image.png'

            shipment_id = item.get('shipment')

            s = () ->
              o = ''
              if ! parseInt(item.get('quantity'), 10) > 0
                o = 'hide'
              else
                o = 'show'
              o

            p.push '<li class="'+s()+' m--product product--'+item.get('product')+' m--shipment-item shipment-item--'+item.get('id')+'">
                      <div class="m-image">
                        <img class="m-image" src="' + image + '" alt="Drag '+item.get('name')+'" />
                      </div>
                      <div class="m-name">' + item.get('name').replace(/\((.+)\)/, "") + '</div>
                      <ul class="m-control">
                        <li>
                          <div class="m m--control">
                            <div class="m-title">
                              <label for="shipmentitem-'+item.get('id')+'">
                                QTY
                              </label>
                            </div>
                            <div class="m-body">
                              <input title="Add or remove items of this product" class="quantity" readonly="readonly" id="shipmentitem-'+item.get('id')+'" data-product="'+item.get('product')+'" data-shipment="'+item.get('shipment')+'" data-shipmentitem="'+item.get('id')+'" value="'+item.get('quantity')+'" />
                              <span class="quantity-add button" title="Add One"></span>
                              <span class="quantity-remove button" title="Remove One"></label></span>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>'

          p.push '</ul>'

          shipment.products_list = p.join ''

          month_list.push '<tr class="assignment-panel">
            <td class="month-label">
              <div data-hint="' + shipment.full_date + ', ' + shipment.timeago + '" class="day hint--top hint--info hint--rounded">' + shipment.day + '</div>
              <div class="month">' + shipment.month + '</div>
              <div class="year">' + shipment.year + '</div>
            </td>
            <td class="products-list">
              <div class="m m--products">' + shipment.products_list + '</div>
            </td>
            <td class="price-comparison">
              <dl>
                <dt class="price-term">85</dt>
                <dd>Retail</dd>
                <!--
                  <dt>0</dt>
                  <dd>Online</dd>
                -->
                <dt class="price-term">' + shipment.price + '</dt>
                <dd>ePantry</dd>
              </dl>
            </td></tr>'

          month_panel$ = $ month_list.join ''

        $('.m-calendar').append month_panel$
      )

    render: () ->

      template = _.template $("#page--pantry").html()

      @$el.html template @customer_data

    events: () ->
      "dropon .assignment-panel": "month_dropon"
      "dropend .assignment-panel": "month_dropend"
      "dropout .assignment-panel": "month_dropout"
      "dropover .assignment-panel": "month_dropover"
      "draginit .m--shipment-item": "product_drag"
      "dragmove .m--shipment-item": "product_move"
      "dragend .m--shipment-item": "product_dragend"
      "change .m--shipment-item input": "update_product"
      "click .m--shipment-item": "shipment_item_click"
      "click .m--shipment-item .quantity-add": "update_shipment"
      "click .m--shipment-item .quantity-remove": "update_shipment"
      "click .reveal-months": "reveal_months"
      "click .reveal-checkout": "reveal_checkout"

    shipment_item_click: (e) ->

      el$ = $ @

      el$.addClass 'drag'

    reveal_months: (e) ->

      e.preventDefault()

      _e e

    reveal_checkout: (e) ->

      e.preventDefault()

      if ! @payment_view

        @payment_view = new eP.App.Views.PaymentView({
            el: $ '.app-modals'
            customer: @working_customer
        })

      else

        @payment_view.initialize()

    update_shipment: (e) ->

      # build object
      
      el$ = $ e.target
      control$ = el$.closest '.m--control'
      i$ = control$.find 'input'
      product = i$.attr 'data-product'
      shipment = i$.attr 'data-shipment'
      shipmentitem = i$.attr 'data-shipmentitem'

      v = i$.val()

      if parseInt v, 10 > 0 && parseInt v, 10 < 10

        if el$.hasClass 'quantity-add'
          ++v

        if el$.hasClass 'quantity-remove'
          --v

        shipment_item = new eP.App.Collections.ShipmentItems

        shipment_item.add [{
          id: shipmentitem
        }]

        shipment_item.at(0).url = '/api/shipmentitem/update/' + shipmentitem + '/'

        shipment_update = shipment_item.at(0).save({
          quantity: v
        }, {
          patch: true
          success: (model, response, options) ->
            i$.val(model.get 'quantity')
        })

      else

        error_view = new eP.App.MicroViews.ErrorView {
          content: msg
        }

    month_dropout: (e, drop, drag) ->

      drop.element.removeClass 'over'

    month_dropend: (e, drop, drag) ->

      drop.element.removeClass 'over'

      drag.element.remove()

    month_dropon: (e, drop, drag) ->

      # drag
      drag$ = drag.element
      container$ = drag$.closest('.m-body')
      drag$.removeClass 'drag'
      control$ = drag$.find '.m--control'
      i$ = control$.find 'input'
      product = i$.attr 'data-product'
      shipment = i$.attr 'data-shipment'
      shipment_item = i$.attr 'data-shipmentitem'

      # drop
      drop$ = drop.element
      drop_id = drop$.find('.m-body').attr 'data-shipment'

      # shipment
      shipment_items = new eP.App.Collections.ShipmentItems {
        id: shipment_item
        product: product
        quantity: i$.val()
      }


      if drop$.find('.product--' + product).hasClass('show')

        # we've found a hidden dupe

        shipment_items.at(0).url = '/api/shipmentitem/update/' + shipment_item + '/'

        shipment_update = shipment_items.at(0).save({
          shipment: drop_id
        }, {
          #success: (model, response, options) ->
            #drop$.find('.m--products > ul.m-body').append drag.element[0].outerHTML

          error: (model, xhr, options) ->
            msg = xhr.responseText

            error_view = new eP.App.MicroViews.ErrorView {
              content: msg
            }

            container$.append drag$[0].outerHTML
        })

      else if drop$.find('.hide.product--' + product).length

        shipment_items.at(0).url = '/api/shipmentitem/update/' + drop$.find('input[data-product=' + product + ']').attr('data-shipmentitem') + '/'

        shipment_update = shipment_items.at(0).save({
          quantity: i$.val()
        }, {
          patch: true
        })

        drag$.after drag$[0].outerHTML

        drop$.find('.m--products > ul.m-body').append drag$[0].outerHTML

    month_dropover: (e, drop, drag) ->

      drop.element.addClass 'over'

    product_dragend: (e, drag) ->

      drag.element.removeClass 'drag'

    product_move: (e, drag) ->

      drag.element.addClass 'drag'

    product_drag: (e, drag) ->

      drag.element.addClass 'drag'

      drag.ghost()

    update_product: (e) ->

      target$ = $ e.target

      @pantries.at(0).save()
 
