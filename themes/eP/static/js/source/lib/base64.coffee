# eP universe
window.Base64 ?= {}

Base64._keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=@"
Base64.encode = (input) ->
  o = ''
  chr1 = ''
  chr2 = ''
  chr3 = ''
  enc1 = ''
  enc2 = ''
  enc3 = ''
  enc4 = ''
  i = 0

  input = Base64._utf8_encode(input)

  while i < input.length
    chr1 = input.charCodeAt(i++)
    chr2 = input.charCodeAt(i++)
    chr3 = input.charCodeAt(i++)

    enc1 = chr1 >> 2
    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4)
    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6)
    enc4 = chr3 & 63

    if isNaN(chr2)
      enc3 = enc4 = 64
    else if isNaN(chr3)
      enc4 = 64

    o = o
    + @keys.charAt(enc1) + @keys.charAt(enc2)
    + @keys.charAt(enc3) + @keys.charAt(enc4)
  o

Base64.decode = (input) ->
  #

Base64._utf8_encode = (str) ->
Base64._utf8_decode = (str) ->

