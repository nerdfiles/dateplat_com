// Generated by CoffeeScript 1.7.1
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  module.exports = function() {
    return eP.App.Views.PantryView = (function(_super) {
      __extends(PantryView, _super);

      function PantryView() {
        return PantryView.__super__.constructor.apply(this, arguments);
      }

      PantryView.prototype.close = function() {
        this.$el.empty();
        return this.undelegateEvents();
      };

      PantryView.prototype.initialize = function() {
        var app_intro$, loaded_customer, nice_work_modal$;
        this.reveal_defaults = {
          closeOnBackgroundClick: false,
          animation: 'fade'
        };
        $('.m--progress').fadeOut().remove();
        app_intro$ = $('.app-intro').detach();
        nice_work_modal$ = $('#nice-work');
        nice_work_modal$.trigger('reveal:close');
        this.email = this.options.customer_email;
        this.sign_up_customer = this.options.sign_up_customer;
        $('html,body').addClass('eP-dashboard');
        $('.app--page').removeClass('landing');
        this.render();
        if (this.sign_up_customer) {
          this.customers = this.sign_up_customer;
          this.customers.bind('change', this.update_dashboard, this);
          return this.customers = this.customers.save();
        } else {
          loaded_customer = new eP.App.Models.Customer({
            id: this.email
          });
          loaded_customer.bind('change', this.update_dashboard, this);
          this.customers = loaded_customer.fetch();
          return this.customers.done((function(_this) {
            return function(customer) {
              _this.customers = new eP.App.Collections.Customers;
              return _this.customers.reset([customer]);
            };
          })(this));
        }
      };

      PantryView.prototype.update_dashboard = function(customer) {
        var pantry_calendar$;
        this.working_customer = customer;
        this.$el.empty();
        this.render();
        pantry_calendar$ = this.$el.find('.m.m--pantry-calendar');
        return $('.m--pantry-calendar .loader').animate({
          opacity: 0
        }, 250, function() {
          var creation_date, el$, first_date, loaded_pantry, month_list, month_panel$, prepared_shipments;
          el$ = $(this);
          el$.slideUp();
          loaded_pantry = customer.get('pantry');
          this.loaded_shipments = _.sortBy(loaded_pantry.shipments, function(shipment) {
            return shipment.date;
          });
          first_date = this.loaded_shipments[0].date;
          creation_date = moment(first_date);
          month_list = [];
          month_panel$ = '';
          prepared_shipments = _.reject(this.loaded_shipments, function(shipment, i) {
            if (i >= 12) {
              return shipment;
            }
          });
          _.each(prepared_shipments, (function(_this) {
            return function(shipment) {
              var p, shipment_id, shipments;
              shipment_id = shipment.id;
              shipment.day = moment(shipment.date).format('Do');
              shipment.month = moment(shipment.date).format('MMM');
              shipment.year = moment(shipment.date).format('YYYY');
              shipment.full_date = moment(shipment.date).format('dddd, MMMM Do YYYY');
              shipment.timeago = moment(shipment.date).endOf('day').fromNow();
              shipments = new eP.App.Collections.Shipments(shipment.items);
              shipment.products_list = '';
              p = [];
              p.push('<ul class="m-body" data-shipment="' + shipment_id + '">');
              shipments.each(function(item) {
                var image, s;
                image = item.get('image') && item.get('image') !== 'no image' ? item.get('image') : '/static/img/demo/meyers-test-image.png';
                shipment_id = item.get('shipment');
                s = function() {
                  var o;
                  o = '';
                  if (!parseInt(item.get('quantity'), 10) > 0) {
                    o = 'hide';
                  } else {
                    o = 'show';
                  }
                  return o;
                };
                return p.push('<li class="' + s() + ' m--product product--' + item.get('product') + ' m--shipment-item shipment-item--' + item.get('id') + '"> <div class="m-image"> <img class="m-image" src="' + image + '" alt="Drag ' + item.get('name') + '" /> </div> <div class="m-name">' + item.get('name').replace(/\((.+)\)/, "") + '</div> <ul class="m-control"> <li> <div class="m m--control"> <div class="m-title"> <label for="shipmentitem-' + item.get('id') + '"> QTY </label> </div> <div class="m-body"> <input title="Add or remove items of this product" class="quantity" readonly="readonly" id="shipmentitem-' + item.get('id') + '" data-product="' + item.get('product') + '" data-shipment="' + item.get('shipment') + '" data-shipmentitem="' + item.get('id') + '" value="' + item.get('quantity') + '" /> <span class="quantity-add button" title="Add One"></span> <span class="quantity-remove button" title="Remove One"></label></span> </div> </div> </li> </ul> </li>');
              });
              p.push('</ul>');
              shipment.products_list = p.join('');
              month_list.push('<tr class="assignment-panel"> <td class="month-label"> <div data-hint="' + shipment.full_date + ', ' + shipment.timeago + '" class="day hint--top hint--info hint--rounded">' + shipment.day + '</div> <div class="month">' + shipment.month + '</div> <div class="year">' + shipment.year + '</div> </td> <td class="products-list"> <div class="m m--products">' + shipment.products_list + '</div> </td> <td class="price-comparison"> <dl> <dt class="price-term">85</dt> <dd>Retail</dd> <!-- <dt>0</dt> <dd>Online</dd> --> <dt class="price-term">' + shipment.price + '</dt> <dd>ePantry</dd> </dl> </td></tr>');
              return month_panel$ = $(month_list.join(''));
            };
          })(this));
          return $('.m-calendar').append(month_panel$);
        });
      };

      PantryView.prototype.render = function() {
        var template;
        template = _.template($("#page--pantry").html());
        return this.$el.html(template(this.customer_data));
      };

      PantryView.prototype.events = function() {
        return {
          "dropon .assignment-panel": "month_dropon",
          "dropend .assignment-panel": "month_dropend",
          "dropout .assignment-panel": "month_dropout",
          "dropover .assignment-panel": "month_dropover",
          "draginit .m--shipment-item": "product_drag",
          "dragmove .m--shipment-item": "product_move",
          "dragend .m--shipment-item": "product_dragend",
          "change .m--shipment-item input": "update_product",
          "click .m--shipment-item": "shipment_item_click",
          "click .m--shipment-item .quantity-add": "update_shipment",
          "click .m--shipment-item .quantity-remove": "update_shipment",
          "click .reveal-months": "reveal_months",
          "click .reveal-checkout": "reveal_checkout"
        };
      };

      PantryView.prototype.shipment_item_click = function(e) {
        var el$;
        el$ = $(this);
        return el$.addClass('drag');
      };

      PantryView.prototype.reveal_months = function(e) {
        e.preventDefault();
        return _e(e);
      };

      PantryView.prototype.reveal_checkout = function(e) {
        e.preventDefault();
        if (!this.payment_view) {
          return this.payment_view = new eP.App.Views.PaymentView({
            el: $('.app-modals'),
            customer: this.working_customer
          });
        } else {
          return this.payment_view.initialize();
        }
      };

      PantryView.prototype.update_shipment = function(e) {
        var control$, el$, error_view, i$, product, shipment, shipment_item, shipment_update, shipmentitem, v;
        el$ = $(e.target);
        control$ = el$.closest('.m--control');
        i$ = control$.find('input');
        product = i$.attr('data-product');
        shipment = i$.attr('data-shipment');
        shipmentitem = i$.attr('data-shipmentitem');
        v = i$.val();
        if (parseInt(v, 10 > 0 && parseInt(v, 10 < 10))) {
          if (el$.hasClass('quantity-add')) {
            ++v;
          }
          if (el$.hasClass('quantity-remove')) {
            --v;
          }
          shipment_item = new eP.App.Collections.ShipmentItems;
          shipment_item.add([
            {
              id: shipmentitem
            }
          ]);
          shipment_item.at(0).url = '/api/shipmentitem/update/' + shipmentitem + '/';
          return shipment_update = shipment_item.at(0).save({
            quantity: v
          }, {
            patch: true,
            success: function(model, response, options) {
              return i$.val(model.get('quantity'));
            }
          });
        } else {
          return error_view = new eP.App.MicroViews.ErrorView({
            content: msg
          });
        }
      };

      PantryView.prototype.month_dropout = function(e, drop, drag) {
        return drop.element.removeClass('over');
      };

      PantryView.prototype.month_dropend = function(e, drop, drag) {
        drop.element.removeClass('over');
        return drag.element.remove();
      };

      PantryView.prototype.month_dropon = function(e, drop, drag) {
        var container$, control$, drag$, drop$, drop_id, i$, product, shipment, shipment_item, shipment_items, shipment_update;
        drag$ = drag.element;
        container$ = drag$.closest('.m-body');
        drag$.removeClass('drag');
        control$ = drag$.find('.m--control');
        i$ = control$.find('input');
        product = i$.attr('data-product');
        shipment = i$.attr('data-shipment');
        shipment_item = i$.attr('data-shipmentitem');
        drop$ = drop.element;
        drop_id = drop$.find('.m-body').attr('data-shipment');
        shipment_items = new eP.App.Collections.ShipmentItems({
          id: shipment_item,
          product: product,
          quantity: i$.val()
        });
        if (drop$.find('.product--' + product).hasClass('show')) {
          shipment_items.at(0).url = '/api/shipmentitem/update/' + shipment_item + '/';
          return shipment_update = shipment_items.at(0).save({
            shipment: drop_id
          }, {
            error: function(model, xhr, options) {
              var error_view, msg;
              msg = xhr.responseText;
              error_view = new eP.App.MicroViews.ErrorView({
                content: msg
              });
              return container$.append(drag$[0].outerHTML);
            }
          });
        } else if (drop$.find('.hide.product--' + product).length) {
          shipment_items.at(0).url = '/api/shipmentitem/update/' + drop$.find('input[data-product=' + product + ']').attr('data-shipmentitem') + '/';
          shipment_update = shipment_items.at(0).save({
            quantity: i$.val()
          }, {
            patch: true
          });
          drag$.after(drag$[0].outerHTML);
          return drop$.find('.m--products > ul.m-body').append(drag$[0].outerHTML);
        }
      };

      PantryView.prototype.month_dropover = function(e, drop, drag) {
        return drop.element.addClass('over');
      };

      PantryView.prototype.product_dragend = function(e, drag) {
        return drag.element.removeClass('drag');
      };

      PantryView.prototype.product_move = function(e, drag) {
        return drag.element.addClass('drag');
      };

      PantryView.prototype.product_drag = function(e, drag) {
        drag.element.addClass('drag');
        return drag.ghost();
      };

      PantryView.prototype.update_product = function(e) {
        var target$;
        target$ = $(e.target);
        return this.pantries.at(0).save();
      };

      return PantryView;

    })(Backbone.View);
  };

}).call(this);
