# -*- coding: utf-8 -*- #

from fabric.api import env, local, require

'''
  contextualized fabric functions

  @usage            $ fab [env] [command]
  @nerdfiles
  @date             02 2013 02 05:36:08
'''

def deploy():
  require('environment')
  maintenance_on()
  push()
  syncdb()
  migrate()
  maintenance_off()
  ps()

def maintenance_on():
  require('environment')
  local('heroku maintenance:on --remote %s' % env.environment)

def maintenance_off():
  require('environment')
  local('heroku maintenance:off --remote %s' % env.environment)

def push():
  require('environment')
  local('git push %s master' % env.environment)

def syncdb():
  require('environment')
  if(env.environment == "development"):
    local('foreman run python manage.py syncdb')
  else:
    local('heroku run python manage.py syncdb --remote %s' % env.environment)

def ps():
  require('environment')
  local('heroku ps --remote %s' % env.environment)

def open():
  require('environment')
  local('heroku open --remote %s' % env.environment)

def migrate(app=None):
  require('environment')
  if(env.environment == "development"):
    if(app is not None):
      local('foreman run python manage.py migrate %s' % app)
    else:
      local('foreman run python manage.py migrate')
  else:
    if(app is not None):
      local('heroku run python manage.py migrate %s --remote %s' % (app, env.environment))
    else:
      local('heroku run python manage.py migrate --remote %s' % env.environment)

# $ fab migrate_schema:[app] 

def migrate_schema(app):
  local('foreman run "python manage.py schemamigration %s --auto"' % app)


'''
  contextualizers  

  @nerdfiles
'''

def development():
  env.environment = 'development'

def staging():
  env.environment = 'staging'

def production():
  env.environment = 'production'
