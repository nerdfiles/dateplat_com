key_enter = 13
key_backspace = 8

window.eP ?= {}

stripeResponseHandler = (status, response) ->

  if (response.error)

    if ($(".payment-errors").length)
      $(".payment-errors").remove()

    $(".payment-errors").html(response.error.message)

    $(".button").removeAttr("disabled")

  else

    form$ = $(".payment-form")

    token = response['id']

    form$.append "<input type='hidden' name='stripeToken' value='" + token + "'/>"

    # LMAO
    stripeToken = form$.serialize()

    $.ajax {
      type: 'POST'
      url: '../api/shipment/charge/'
      data: stripeToken
      success: () ->
        _e 'success'
      error: () ->
        _e 'failure'
    }


_e = (thing) ->
  console.log thing


String.prototype.is_email = () ->
  o = ''
  reg_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
  if reg_email.test @
    o = @
  o.toLowerCase()


Array.prototype.clean = () ->
  for i in [0...@length]
    if (_.isEmpty @[i])
      @splice(i, 1)
      i--
  @


eP.Bones = () ->

    
  # Answer Model
  
  class eP.App.Models.Answer extends Backbone.Model
    
    defaults:
      id: null


  # Brand Model

  class eP.App.Models.Brand extends Backbone.Model

    defaults:
      id: null

    url: () ->
      "/api/brandtypes/"


  # Customer Model
  
  class eP.App.Models.Customer extends Backbone.Model

    defaults:
      email: null
      pantry: null

    url: () ->
      if @.isNew()
        u = '/api/customer/create/'
      else
        u = 'api/customer/' + @email.id
      u

    initialize: (id) ->
      @email = id

  # Question Model

  class eP.App.Models.Question extends Backbone.Model

    defaults:
      id: null
      position: null
      title: null
      image: null
      text: null
      answers: null

    url: ->
      "/api/questions/"


  # Preference Model
  
  class eP.App.Models.Preference extends Backbone.Model

    defaults:
      id: null


  # Pantry Model
  
  class eP.App.Models.Pantry extends Backbone.Model

    defaults:
      answers: null
      preferences: null
      products: null
      excluded_products: null
 
    url: () ->
      '/api/pantries/'

    initialize: () ->
      @bind('change', @update)

    update: () ->
      # console.log @


  # Product Model

  class eP.App.Models.Product extends Backbone.Model

    url: () ->
      "/api/categories/"

    defaults:
      name: null
      canonical_image: "static/img/demo/meyers-test-image.png"


  # Shipment Item Model

  class eP.App.Models.ShipmentItem extends Backbone.Model

    defaults:
      id: null
      product: null
      shipment: null
      quantity: null

    url: () ->
      "/api/shipmentitem/update/"


  # Shipment Model

  class eP.App.Models.Shipment extends Backbone.Model
  
    defaults:
      id: null
      product: null # ID
      shipment: null # ID
      quantity: null # Int

    url: () ->
      "/api/shipment/charge/"


  # Questions Collection

  class eP.App.Collections.Questions extends Backbone.Collection

    model: eP.App.Models.Question # a spec of Questions

    url: () ->
      "/api/questions/"


  # Answers Collection

  class eP.App.Collections.Answers extends Backbone.Collection
    
    model: eP.App.Models.Answer


  # Customer Collection
  
  class eP.App.Collections.Customers extends Backbone.Collection

    model: eP.App.Models.Customer

    url: () ->

      '/api/customer/'


  # Shipment Items Collection

  class eP.App.Collections.ShipmentItems extends Backbone.Collection

    model: eP.App.Models.ShipmentItem

    url: () ->
      "/api/shipmentitem/update/"


  # Shipments Collection
  
  class eP.App.Collections.Shipments extends Backbone.Collection

    model: eP.App.Models.Shipment


  # Preferences Collection
  
  class eP.App.Collections.Preferences extends Backbone.Collection

    model: eP.App.Models.Preference

 
  # Pantries Collection

  class eP.App.Collections.Pantries extends Backbone.Collection

    model: eP.App.Models.Pantry

    url: () ->

      "/api/customer/"


  # Products Collection

  class eP.App.Collections.Products extends Backbone.Collection

    model: eP.App.Models.Product

    url: () ->
      "/api/categories/"

    initialize: () ->

      @fetch({
        update: true
        remove: true
        success: (response, data) =>
          @reset data
      })


  # Brands Collection

  class eP.App.Collections.Brands extends Backbone.Collection

    model: eP.App.Models.Brand

    url: () ->
      "/api/brandtypes/"


  class eP.App.Routers.BaseWorkspace extends Backbone.Router

    routes:
      "": "base_router"
      "customized-schedule": "reveal_customized_schedule"
      "pantry-create": "create_pantry"
      "pantry-login": "login_pantry"
      "pantry-sign-up": "pantry_sign_up"
      "question-:position": "reveal_question"
      "essential-:type": "position_essential_orbital"
      "specialty-:type": "position_specialty_orbital"
      "faq": "reveal_faq"
      "discovery-bulk": "reveal_discovery_bulk"
      "discovery-prices": "reveal_discovery_prices"
      "brand-:type": "position_brand"
      "preference-:type-:brand_slug-:pref_slug": "position_preference"
      "thanks-for-the-:type": "position_brand_cycle"

    initialize: () ->

      @progress_bar_weights =
        question: 2.5
        preference: 3
        essential: 3
        specialty: 3
        brand: 3

      @route /^(\/)?([\w-\.]+@([\w-]+\.)+[\w-]{2,4})\/pantry(\/)?$/, "open_pantry"

      @reveal_defaults =
        closeOnBackgroundClick: false
        animation: 'fade'

    reveal_faq: () ->
      $('#faq').reveal @reveal_defaults

    reveal_discovery_bulk: () ->
      $('#discovery-bulk').reveal @reveal_defaults

    reveal_discovery_prices: () ->
      $('#discovery-prices').reveal @reveal_defaults

    open_pantry: (dir, name) ->

      @pantry_view = new eP.App.Views.PantryView({
        el: $(".app-content .app--page")
        customer_email: name
      })

    base_router: () ->

      app_header$ = $ '.app-header'

      app_content$ = $ '.app-content'

      $('.reveal-modal.open').trigger('reveal:close')

      app_header$.waypoint((direction) ->
        #console.log direction
        #app_header$.toggleClass 'fixed'
      )

      app_content$.find('h1').waypoint((direction) ->
        #console.log direction
        #app_content$.find('h1').addClass 'fixed'
      )

    position_brand_selection: () ->

      $('.reveal-modal.open').trigger('reveal:close')

    position_brand_cycle: (type) ->

      $('#thanks-for-the-' + type).reveal @reveal_defaults

    position_preference: (type, brand_slug, pref_slug) ->
      _type = type.split('-')
      type = _type[0]
      pref_slug = brand_slug + '-' + pref_slug
      brand_slug = _type[1]
      _brand_slug = if brand_slug then (brand_slug + '-') else ''
      $('#preference-'+type+'-'+brand_slug+'-'+pref_slug).filter(':not([id*="scents"])').reveal @reveal_defaults

      $('.m--progress ul .preference-'+type).remove()

      $('.m--progress ul').append $('<li class="preference-'+pref_slug+' slice">.</li>')

      $('.m--progress span').text (15+$('.m--progress li').length * @progress_bar_weights.preference)+'%'

    position_specialty_orbital: (type) ->

      $('.reveal-modal.open').trigger('reveal:close')

      $('.app-content .app--page .brand-picks').hide()

      $('#specialty-'+type).show()

      $('.m--progress ul .specialty-'+type).remove()

      $('.m--progress ul').append $('<li class="specialty-'+type+' slice">.</li>')

      $('.m--progress span').text (15+$('.m--progress li').length * @progress_bar_weights.specialty)+'%'

      @current_orbital_view = $('body').data 'current_orbital_view'

      if @current_orbital_view

        @events =
          'click .brand-orbital .remove': 'remove_product'
          'click .brand-orbital .m--product .m-title': 'reveal_product'
          'click .brand-orbital .m--navigation .next': 'save_pantry'
          'click .brand-orbital .m--navigation .back': 'position_previous'

        @current_orbital_view.delegateEvents @events

        $('body').data 'current_orbital_view', null

    position_essential_orbital: (type) ->

      $('.reveal-modal.open').trigger('reveal:close')

      $('.app-content .app--page .brand-picks').hide()

      $('#essential-'+type).show()

      $('.m--progress ul .essential-'+type).remove()

      $('.m--progress ul').append $('<li class="essential-'+type+' slice">.</li>')

      $('.m--progress span').text (15+$('.m--progress li').length * @progress_bar_weights.essential)+'%'

      @last_orbital_view = $('body').data 'last_orbital_view'

      if @last_orbital_view

        @last_orbital_view.initialize()

        $('body').data 'last_orbital_view', null

    position_brand: (type) ->

      brand_picks$ = $('#brand-'+type).detach()

      $('.app-content .app--page .brand-orbital').hide()

      $('.app-content .app--page').prepend brand_picks$

      $('.app-history').hide()

      $('.app-content .app--page').show()

      $('#brand-'+type).show()

      $('.m--progress ul .brand-'+type).remove()

      $('.m--progress ul').append $('<li class="brand-'+type+' slice">.</li>')

      $('.m--progress span').text (15+$('.m--progress li').length * @progress_bar_weights.brand)+'%'

      #@progress_breadcrumb_view.progress(type, 2.5)

    reveal_customized_schedule: () ->

      $('.reveal-modal.open').trigger('reveal:close')

      reveal_settings = _.clone @reveal_defaults

      reveal_settings.close = () ->
        window.location.hash = ''

      $('#customized-schedule').reveal reveal_settings

    reveal_question: (position) ->

      frag = Backbone.history.fragment

      $('.app-intro').hide()
      $('.app-content .app--page').hide()

      $('.app-history').show()

      $('.wizard-question').hide()

      $('#question-' + position).delay(200).css({
        right: "-9999px"
        display: "block"
        opacity: 0
      }).animate({
        right: "0px"
        opacity: 1
      }, 350)

      @progress_breadcrumb_view = new eP.App.MicroViews.ProgressBreadcrumbView({
          el: $('.app-header')
          position: position
          weight: @progress_bar_weights.question
      })

    login_pantry: () ->

      if ! @wizard_pantry_intro_view

        @wizard_pantry_sign_up_view = new eP.App.ModalViews.PantrySignUpView({
            el: $(".app-modals")
            login: true
        })

      else

        @wizard_pantry_sign_up_view.initialize()

      $('#pantry-sign-up').reveal @reveal_defaults

    create_pantry: () ->

      if ! @wizard_pantry_intro_view

        @wizard_pantry_intro_view = new eP.App.ModalViews.PantryIntroView({
            el: $(".app-modals")
        })

      else

        @wizard_pantry_intro_view.initialize()

    pantry_sign_up: () ->

      $('#pantry-sign-up').reveal @reveal_defaults


  # Question View

  class eP.App.AdHocViews.QuestionView extends Backbone.View

    initialize: () ->

      @name = @options.name

      @pantries = @options.pantries

      working_pantry = @pantries.at(0)

      question$ = $('#' + @name)

      if @name == 'question-1'

        answers$ = question$.find('li .question-answer')

        question$.find('ul').hide()

        included_next$ = question$.next().find('.stage')

        question$.find('.stage').after included_next$

        uislider$ = $ '#' + @name + ' .noUiSlider'

        uislider$.show()

        uislider$.noUiSlider({
            range: [0, 8]
          , start: 0
          , handles: 1
          , orientation: "horizontal"
          , step: 1
          , serialization: {
                to: [@name, false]
              , resolution: 1
            }
          , slide: () ->

            i$ = $ @

            v = i$.val()

            answers = working_pantry.get 'answers'

            if ! answers

              answers = new eP.App.Collections.Answer

            else

              answers$.each () ->
                a$ = $ @
                id = a$.attr('data-answer')
                answerable = answers.where { id: parseInt id, 10 }
                answers = answers.remove answerable

            input_answer_obj =
              id: parseInt v, 10

            answers.add input_answer_obj

            working_pantry.set 'answers', answers

            # debug this
            _e working_pantry

        })

        uislider$.find('a').after answers$

      window.location.hash = '#' + @name


  # Preference Wizard View

  class eP.App.AdHocViews.PreferenceWizardView extends Backbone.View

    initialize: () ->

      $('.brand-orbital').addClass 'hide'

      $('.selected-brand').addClass 'show'

    events:

      "click .brand-phase-pref .multiple": "select_scent"

    select_scent: (e) ->

      e.preventDefault()


  # Wizard View

  class eP.App.ModalViews.WizardView extends Backbone.View

    question_modals: new eP.App.Collections.Questions

    close: () ->

      if @brand_phase_view

        @brand_phase_view.close()

      $('.wizard-question').remove()

    initialize: () ->

      $('.reveal-modal.open').trigger('reveal:close')

      @pantries = @options.pantries.collection

      @questions = @options.questions

      @question_modals.add @questions # QMs 

      if @pre__render

        @pre__render()

      if @$el.children('.wizard-question').length

        @$el.children('.wizard-question').remove()

      @render()

      $('.noUiSlider').hide()

      @wizard_questions = @$el.children('.wizard-question')

      @$last_wizard_question = $ _.last @wizard_questions

      @$first_wizard_question = $ _.first @wizard_questions

      if ! @question_view

        @question_view = new eP.App.AdHocViews.QuestionView({
          pantries: @pantries
          name: 'question-1'
        })

      @$input = @$first_wizard_question.find('.stage:eq(0) input[name="'+@$first_wizard_question.attr('id')+'"]')
      @$personal_infantry_input = @$first_wizard_question.find('.stage:eq(1)')

      $('.m--progress').fadeIn()

    render: () ->

      html = []

      l = @questions.length

      $.each @questions, () ->

        model = @

        model.set_size = l

        template = _.template(
          $("#wizard--pantry-question").html()
        )

        html.push( template(model) )

      @$el.prepend( html.join('') )

    events:
      "click .wizard-question .wizard--page-body a": "update_pantry"
      "click .wizard-question .wizard--page-body a:eq(0)": "sanitize_slider"
      "click .wizard-question .question-answer": "clear_errors"
      "click .wizard-question .back": "position_previous"
      "click .wizard-question .forward": "position_next"
      "dragdown .wizard-question .noUiSlider div": "clear_errors"
      "dragend .wizard-question .noUiSlider div": "clear_errors"

    sanitize_slider: (e) ->

      e.preventDefault()

      el$ = $ ".noUiSlider div"

      el$.removeAttr 'data-hint'
      
    clear_errors: (e) ->

      el$ = $ ".noUiSlider div"

      el$.removeAttr 'data-hint'

      if (parseInt(@$input.val(), 10) != 0 && parseInt(@$input.val(), 10) < 9 && @$personal_infantry_input.find('.selected').length)

        $('.wizard-question .forward').fadeIn().addClass('visible')

      else

        $('.wizard-question .forward').fadeOut().removeClass('visible')

    position_next: (e) ->

      e.preventDefault()

      if parseInt(@$input.val(), 10) != 0

        window.location.hash = '#question-3'

      else

        $('.noUiSlider div').addClass('hint--left hint--warning hint--rounded hint--always').attr('data-hint', 'Help us find out more about your home.')

      $('.wizard-question .forward').fadeOut().removeClass('visible')

    position_previous: (e) ->

      e.preventDefault()

      window.history.back()

    update_pantry: (e) ->

      e.preventDefault()

      a$ = $ e.currentTarget

      $('.wizard-question .forward').fadeOut().removeClass('visible')

      container$ = a$.closest('ul')

      all_a$ = container$.find('a')

      input_answer = a$.attr('data-answer')

      if ! input_answer

        input_answer = @$input.val()

      answers = @pantries.at(0).get 'answers'

      if ! answers

        answers = new eP.App.Collections.Answer

      else

        _.each all_a$, (el) ->

          el$ = $ el

          if el$.hasClass('selected')

            el$.removeClass('selected')

            input_object =
              id: parseInt el$.attr('data-answer'), 10

            answers.remove input_object

      if input_answer != null

        a$.addClass 'selected'

        input_answer_obj =
          id: parseInt input_answer, 10

        answers.add input_answer_obj

        @pantries.at(0).set 'answers', answers

        current_wizard_question$ = a$.closest('.wizard-question')

        if (current_wizard_question$.attr('id') != @$first_wizard_question.attr('id'))

          $('.wizard-question .forward').fadeOut().removeClass('visible')

        input$ = @$first_wizard_question.find('.stage:eq(0) input[name="'+@$first_wizard_question.attr('id')+'"]')

        if ((parseInt(input$.val(), 10) > 0 && parseInt(input$.val(), 10) < 9) &&
            @$personal_infantry_input.find('.selected').length)

          if (current_wizard_question$.attr('id') == @$last_wizard_question.attr('id'))

            if ! @pre_brand_phase_view

              @pre_brand_phase_view = new eP.App.ModalViews.PreBrandPhaseView({
                  el: $(".app-content .app--page")
                , pantries: @pantries
              })

            else

              @pre_brand_phase_view.initialize()

            window.location.href = a$.attr('href')

          else

            if ! a$.hasClass('question-answer')

              window.location.href = a$.attr('href')

        else

          $('.noUiSlider div')
            .addClass('hint--left hint--warning hint--rounded hint--always')
            .attr('data-hint', 'Help us find out more about your home.')

          $('.wizard-question .forward').removeClass 'visible'


  # Brand Phase Init

  class eP.App.ModalViews.PreBrandPhaseView extends Backbone.View

    initialize: () ->

      @pantries = @options.pantries

      @landing_welcome$ = $('.app--page.landing .page').detach()

      @app_intro$ = $('.app-intro').detach()

      brand_phase_init$ = $('.brand-phase-init').detach()

      @render()

      $('.brand-phase-init').reveal({
        closeOnBackgroundClick: false
        animation: 'fade'
      })

    render: () ->

      template = _.template $("#wizard--brand-phase-init").html()

      @$el.prepend( template )

    events:
      "click .brand-phase-init .next": "start_phases"

    start_phases: (e) ->

      e.preventDefault()

      if ! @brand_phase_view

        @brand_phase_view = new eP.App.Views.BrandPhaseView({
            el: $(".app-content .app--page")
          , pantries: @pantries
          , initial: true
        })

      else

        @brand_phase_view.initialize()


  # Pantry Sign-up

  class eP.App.ModalViews.PantrySignUpView extends Backbone.View

    model: eP.App.Models.Pantry

    pantries: new eP.App.Collections.Pantries

    answers: eP.App.Collections.Answers

    questions: new eP.App.Collections.Questions # of Questions

    initialize: () ->

      @customers = new eP.App.Collections.Customers

      @login = @options.login

      @pantry = new @model({
        answers: new @answers
      })

      @pantries.add [@pantry]

      @render()

    render: () ->

      template = _.template $("#wizard--pantry-sign-up").html()

      @$el.prepend( template )

    events:
      "click #pantry-sign-up .button": "create_pantry"
      "change #pantry-sign-up .add-email": "add_email"
      "keydown #pantry-sign-up .add-email": "cancel_jump"

    cancel_jump: (e) ->

      if e.which == key_enter

        e.preventDefault()

        el$ = $ e.target

        el$.closest('.open').find('.note').remove()

        next$ = el$.closest('.wizard--page-body').next().find('.button')

        next$.trigger('focus')

    add_email: (e) ->

      el$ = $ e.target

      @next$ = el$.closest('.wizard--page-body').next().find('.button')

      email = el$.val().is_email()

      @questions_data ?= {}

      el$.closest('.open').find('.note.error').remove()

      if email

        @next$.addClass('primary').removeClass('secondary').removeClass('error')

        @questions.fetch({

          success: (response, data) =>

            @questions_data = data

            customer =
              email: email
              pantry: @pantries.at(0) # initial starting state for pantry

            @customers.reset [customer]

            working_customer = @customers.at(0)

            # set sign_up_customer
            $('body').data 'working_customer', working_customer

            working_pantry = working_customer.get('pantry')

            if ! @wizard_intro_view
             
              @wizard_intro_view = new eP.App.ModalViews.WizardView({
                  el: $(".app-modals")
                , pantries: working_pantry
                , questions: @questions_data
              })

            else

              $('.reveal-modal.open').trigger('reveal:close')

        })

      else

        @next$.removeClass('primary').addClass('secondary').addClass('error')

        el$.parent().addClass('hint--left hint--warning hint--rounded hint--always').attr('data-hint', 'We\'ll need a valid e-mail address!')

    create_pantry: (e) ->

      e.preventDefault()

      el$ = $ e.target

      wiz = $('body').data('wizard_intro')

      start_wizard = el$.attr 'href'

      if @wizard_intro_view && el$.hasClass('primary')

        if ! wiz

          $('body').data('wizard_intro', @wizard_intro_view)

      if @wizard_intro_view

        if ! @login

          window.location.hash = start_wizard

        else

          _e 'check'


  # Create Pantry Intro

  class eP.App.ModalViews.PantryIntroView extends Backbone.View

    initialize: () ->

      @render()

      $('#create-pantry').reveal()

    render: () ->

      template = _.template $("#wizard--create-pantry").html()

      @$el.prepend( template )

    events:
      "click #create-pantry .next": "pantry_sign_up"

    pantry_sign_up: (e) ->

      e.preventDefault()

      el$ = $ e.target

      wizard_pantry_sign_up_view = new eP.App.ModalViews.PantrySignUpView({
          el: $(".app-modals")
      })

      window.location.hash = el$.attr('href')


  # Pre Brand Orbital Interstitial Modal

  class eP.App.ModalViews.PreBrandOrbitalView extends Backbone.View

    initialize: () ->

      product_removal$ = $('#product-removal').detach()

      body$ = $('body')

      @render()

      if ! body$.hasClass('product-removal-closed')

        $('#product-removal').reveal({
          closeOnBackgroundClick: false
          animation: 'fade'
          closed: () ->
            body$.addClass 'product-removal-closed'
        })

    render: () ->

      template = _.template $("#wizard--prep-brand-orbital").html()

      @$el.prepend template

    events:
      "click .product-removal .button": "reveal_orbital"

    reveal_orbital: (e) ->

      e.preventDefault()

      $('.reveal-modal.open').trigger('reveal:close')

      @undelegateEvents()

      
  # Product View

  class eP.App.ModalViews.ProductView extends Backbone.View

    initialize: ->

      @product = @options.data

      previous_product$ = @$el.find('#'+@product.slug).detach()

      @render()

    render: () ->

      template = _.template $("#wizard--product").html()

      @$el.prepend template @product

      $('#'+@product.slug).reveal {
        closeOnBackgroundClick: false
        animation: 'fade'
      }


  # FAQ View

  class eP.App.ModalViews.FAQView extends Backbone.View

    el: $ "body"

    initialize: ->

      @render()

    render: () ->

      template = _.template $("#wizard--faq").html()

      @$el.prepend template


  # Branding Complete View

  class eP.App.ModalViews.BrandingCompleteView extends Backbone.View

    close: () ->

      $('#nice-work').remove()

    initialize: () ->

      @close()

      @pantries = @options.pantries

      @render()

      $('#nice-work').reveal({
        closeOnBackgroundClick: false
        animation: 'fade'
      })

    events:

      "click .reveal-pantry": "reveal_pantry"

    reveal_pantry: (e) ->

      e.preventDefault()

      body$ = $ 'body'

      # recall that sign_up_customer
      working_customer = body$.data 'working_customer'

      # clean up pantry to pass only IDs

      # answers
      
      _answers = @pantries.at(0).get 'answers'

      @pantries.at(0).set 'answers', _answers.pluck 'id'

      # preferences
      
      _preferences = @pantries.at(0).get 'preferences'

      @pantries.at(0).set 'preferences', _preferences.pluck 'id'

      # excluded products
      
      _excluded_products = @pantries.at(0).get 'excluded_products'

      if _excluded_products == null

        _excluded_products = []

        @pantries.at(0).set 'excluded_products', _excluded_products

      else

        @pantries.at(0).set 'excluded_products', _.pluck _excluded_products, 'id'

      # filter and clean up products
      
      _products = @pantries.at(0).get 'pantry_products'

      _products_filtered = _.difference _products, _.pluck _excluded_products, 'id'

      @pantries.at(0).set 'products', _products_filtered

      @pantries.at(0).unset 'temp_products'

      @pantries.at(0).unset 'pantry_products'

      working_customer.set 'pantry', @pantries.at(0)

      # set sign_up_customer after sanitization
      body$.data 'working_customer', working_customer

      if ! @pantry_view

        Backbone.history.navigate working_customer.get('email')+'/pantry/'

        @pantry_view = new eP.App.Views.PantryView({
          el: $(".app-content .app--page")
          customer_email: working_customer.get('email')
          sign_up_customer: working_customer
        })

    render: () ->

      template = _.template $("#wizard--branding-complete").html()

      @$el.prepend( template )


  # Brand Phase View

  class eP.App.Views.BrandPhaseView extends Backbone.View

    brands: new eP.App.Collections.Brands

    products: eP.App.Collections.Products

    initialize: () ->

      $('.brand-phase-pref').removeClass 'show'

      $('.app-history').hide()

      $('.reveal-modal.open').trigger('reveal:close')

      @brand_phase = null

      @current_brand = @options.current_brand || false

      @wizard_questions$ = $('.app-modals .wizard-question').detach()

      $('.app-history').append @wizard_questions$

      brand_picks$ = $('.app-history .brand-picks').detach()

      brand_orbital$ = $('.app-content .app--page .brand-orbital').detach()

      brand_phase$ = $('.app-content .app--page .brand-phase').detach()

      $('.app-modals').append brand_phase$.filter('.brand-phase-pref,.prep-brand')

      @pantries = @options.pantries

      @products = new @products

      @render()

      @$el.show()

    render: () ->

      type = $('body').data('recent_type')

      @brands.fetch({

        success: (response, data) =>

          @brands.reset data

          brands = @brands

          if type

            p = []

            $.each data, () ->

              t = @

              for phase in type

                if t.type == phase
                  p.push(brands.where({
                    type: t.type
                  })[0])

            if @current_brand && @current_brand.length

              @brand_phase = @brands.where({
                type: @current_brand[0]
              })

              @brand_phase = @brand_phase[0].attributes

            else

              brands_excluded = @brands.remove(
                p
              )
              
              @brand_phase = brands_excluded.models[0].attributes

          else

            @brand_phase = brands.models[0].attributes

          if @brand_phase != null

            brands = @brand_phase.brands.map (brand) =>
              brand.products = @products.where({
                brand_slug: brand.slug
              })
        
          template = _.template(
            $("#wizard--brand-phase").html()
          )

          page_title = switch
            when @brand_phase.type == "soap" then "Earth's Best Cleaning Supplies"
            when @brand_phase.type == "paper" then "Household Paper"
            else ''

          @brand_phase.page_title = page_title

          t = template({ b: @brand_phase })

          @$el.append( t )

          window.location.hash = "#brand-" + @brand_phase.type

        error: (error_response) =>
          #console.log error_response

      })

    events: {

        "click .brand-picks .add-brand": "add_brand"
      , "click .brand-phase-info .next": "add_brand_from_info"

      , "click .brand-phase-pref .next": "reveal_pref"
      , "click .brand-phase-pref .single": "add_prefs"
      , "click .brand-phase-pref .multiple": "add_prefs"

      , "click .brand-phase.prep-brand .skip": "prepare_pantry"
      , "click .brand-phase.prep-brand .next": "cycle_brand"

      , "click .brand-phase-pref .back": "position_previous"

    }

    prepare_pantry: (e) ->

      e.preventDefault()

      branding_complete_view = new eP.App.ModalViews.BrandingCompleteView({
          el: $(".app-modals")
        , pantries: @pantries
      })

    position_previous: (e) ->

      e.preventDefault()

      $('.reveal-modal.open').removeClass('deferred')

      window.history.back()

    cycle_brand: (e) ->
      
      e.preventDefault()

      @undelegateEvents()

      brand_phase_view = new eP.App.Views.BrandPhaseView({
          el: $(".app-content .app--page")
        , pantries: @pantries
      })

    add_prefs: (e) ->
      
      e.preventDefault()

      el$ = $ e.currentTarget
      
      el$.toggleClass 'selected'

      recent_type = $('body').data('recent_type')

      preferences = @pantries.at(0).get 'preferences'

      pref =
        id: parseInt el$.attr('data-value'), 10

      if preferences

        preferences.remove(pref)

      else

        preferences = new eP.App.Collections.Preferences

      if el$.hasClass 'selected'

        preferences.add pref

      @pantries.at(0).set 'preferences', preferences

    reveal_pref: (e) ->

      e.preventDefault()

      el$ = $ e.currentTarget

      recent_type = $('body').data('recent_type')

      brand_phase_pref$ = el$.closest('.brand-phase-pref')

      current_modal$ = el$.closest('.reveal-modal.open')

      next_brand_phase_pref$ = brand_phase_pref$.filter(':not([id*="info"])').next()

      next_btn$ = $('.brand-orbital .m--navigation .next')

      if brand_phase_pref$.hasClass('last')

        window.location.hash = '#thanks-for-the-' + recent_type

      else

        if ! brand_phase_pref$.hasClass('deferred')

          # todo scents flow (so or adhoc view for preferences -- wizard question that takes up the entire page)
          # 09 2013 05 15:46:02
          
          if next_brand_phase_pref$.hasClass('pref-type-favorite-scents')

              multiple_preference_view = new eP.App.AdHocViews.PreferenceWizardView({
                  el: $('.app-content')
                  pantries: @pantries
              })

          else
            
            window.location.hash = '#' + next_brand_phase_pref$.attr('id')


        else

          current_modal$.trigger('reveal:close')

          next_btn$.trigger('click')

    add_brand: (e) ->
 
      e.preventDefault()

      target$ = $ e.currentTarget # img

      brand_modal$ = target$.closest '.reveal-modal'

      brand_modal$.trigger 'reveal:close'

      brand = target$.attr 'data-slug'

      brand_id = target$.attr 'data-brand-id'

      type = []

      type.push target$.attr 'data-brand-type'

      $('body').data 'recent_type', type

      $('body').data 'recent_brand', brand

      # @note think of preferences as a series of toggles (in terms of how they are presented in the DOM)
      # 09 2013 05 10:22:50
      $('.brand-phase-pref').filter('[id*="'+brand+'"]').filter(':not([class*="reveal-modal"])').addClass 'selected-brand'

      branded_products = []

      @products.map((p) ->

        a = _.pick p, 'attributes'

        filtered_products = []

        products = _.pick a.attributes, 'products'

        filtered_products = _.reject products.products, (product) ->

          if product.brand != parseInt brand_id, 10

            product

        branded_products.push filtered_products

      )

      @pantries.at(0).set 'temp_products', branded_products

      pantry_products = @pantries.at(0).get 'pantry_products'

      clean_products = _.flatten branded_products

      products_unrefilled = _.reject clean_products, (refill) ->
        if refill.parent_product != null
          refill

      if ! pantry_products

        pantry_products = _.pluck products_unrefilled, 'id'

      else

        pantry_products = _.union pantry_products, _.pluck products_unrefilled, 'id'

      @pantries.at(0).set 'pantry_products', pantry_products

      if brand != 'grab-green'

        if ! @brand_orbital_view

          @brand_orbital_view = new eP.App.Views.BrandOrbitalView({
              el: $(".app-content .app--page")
            , pantries: @pantries
            , products: branded_products
            , brands: @brands
            , initial: @options.initial
          })

        else

          @brand_orbital_view.initialize()

      else

        availability$ = target$.closest('li').find('.unavailable')

        if ! availability$.length

          target$.closest('li').prepend $('<p class="unavailable">Currently unavailable.</p>').slideDown()

    add_brand_from_info: (e) ->

      e.preventDefault()

      el$ = $ e.target

      brand_complete$ = el$.closest('.reveal-modal')

      brand_complete$.trigger 'reveal:close'


  # Pantry Pay

  class eP.App.Views.PaymentView extends Backbone.View

    initialize: () ->
      
      # collect DOM body
      body$ = $ 'body'

      # set "state" of document
      body$.addClass 'eP-pay'

      # essentially a "state" of the app modals
      @$el.addClass 'payment'

      # load customer data
      @working_customer = @options.customer
      
      # render payment view data
      @render()

      # collect modal that is [now] present in DOM after @render()
      payment_modal$ = @$el.find '.payment-modal'

      # collect control buttons for re-use
      @btn_back$ = payment_modal$.find '.button.back'
      @btn_pay$ = payment_modal$.find 'button.pay'
      @btn_next$ = payment_modal$.find '.button.next'

      # set "default state" of back button
      @btn_back$.addClass 'hide'

      @payment_reveal_defaults =
        closeOnBackgroundClick: false
        animation: 'fade'
        opened: () =>
          visible$ = payment_modal$.find '.page-nav .visible'
          current_slices$ = payment_modal$.find '.page-body .slices'
          current_slices$.find('li').eq(visible$.index()).addClass 'visible'
          @btn_back$.removeClass 'hide'
          @btn_pay$.parent().removeClass 'show'
          @btn_next$.removeClass 'hide'
        closed: () ->
          body$.removeClass 'eP-pay'
          # closing clears the form and re-renders with loaded data
          $('.billing, .shipping').removeClass 'visible'
          profile$ = payment_modal$.find '.profile'
          profile$.addClass 'visible'

      payment_modal$.reveal @payment_reveal_defaults

    events:
      "click .button.back": "back"
      "click .button.next": "next"
      "click button.pay": "pay"

    render: () ->

      # collect payment template
      template = _.template $("#page--payment").html()

      # load customer into data object for safe template printing
      data =
        customer: @working_customer.toJSON()

      # collect payment modal from DOM
      payment_modal$ = @$el.find '.payment-modal'

      # check in case the payment modal is already in the DOM
      if ! payment_modal$.length
        @$el.append template data
      else
        payment_modal$.reveal @payment_reveal_defaults

    back: (e) ->

      # cancel default link behavior
      e.preventDefault()

      # collect payment modal from DOM
      payment_modal$ = $ '.payment-modal'

      visible_nav_slice$ = payment_modal$.find('.page-nav .visible.slice')

      nav_slices$ = payment_modal$.find '.page-nav .slice'

      content_slices$ = payment_modal$.find '.slices li'

      i = visible_nav_slice$.length

      o = visible_nav_slice$.last().prev().index()

      content_slices$.removeClass 'visible'

      content_slices$.eq(o).addClass 'visible'

      nav_slices$.eq(o).next().removeClass 'visible'

      @btn_pay$.parent().removeClass('show')

      @btn_next$.removeClass 'hide'

    next: (e) ->

      e.preventDefault()

      payment_modal$ = $ '.payment-modal'

      visible_nav_slice$ = payment_modal$.find('.page-nav .visible.slice')

      nav_slices$ = payment_modal$.find '.page-nav .slice'

      content_slices$ = payment_modal$.find '.slices li'

      i = visible_nav_slice$.length

      content_slices$.removeClass 'visible'

      content_slices$.eq(i).addClass 'visible'

      if (i > 1)
        @btn_pay$.parent().addClass 'show'
        @btn_next$.addClass 'hide'

      @btn_back$.removeClass 'hide'

      visible_nav_slice$.next().addClass 'visible'

    pay: (e) ->

      # collect payment modal from DOM
      payment_modal$ = $ '.payment-modal'

      # prep stripe
      meta_token$ = $ "meta[name='stripe-public-key']"

      # exact token
      stripe_token = meta_token$.attr 'content'

      # set extracted token
      Stripe.setPublishableKey stripe_token

      # prep form
      form$ = $ '.payment-form'

      # disable form button to prevent overloaders
      form$.find('.button.pay').prop 'disabled', true

      paying_pantry = @working_customer.get 'pantry'

      paying_shipments = paying_pantry.shipments

      # only prep the first month for payment
      #
      # @note Needs specification.
      first_shipment = _.first(paying_shipments)
        
      # @todo tighten this
      if first_shipment
        form$.append $('<input type="hidden" class="shipment" name="shipment_id" />').val(first_shipment.id)
        form$.append $('<input type="hidden" class="shipment" name="amount" />').val(first_shipment.price)
        form$.append $('<input type="hidden" class="shipment" name="currency" />').val('usd')
        form$.append $('<input type="hidden" class="shipment" name="customer" />').val(first_shipment.pantry)

      # @note When Shipment Diff is set to true, do not data-stripe scrape the billing.
      # Or remove those attributes. The state of the form should always have one subset 
      # attributes applied to one (shipping) or the other (billing).
      Stripe.createToken form$, stripeResponseHandler

      # close payment modal in DOM
      payment_modal$.trigger 'reveal:close'


  # Pantry View

  class eP.App.Views.PantryView extends Backbone.View

    close: () ->

      @$el.empty()

      @undelegateEvents()

    initialize: () ->

      @reveal_defaults =
        closeOnBackgroundClick: false
        animation: 'fade'

      $('.m--progress').fadeOut().remove()
      
      # @note check request headers and do not load include app_intro$
      
      app_intro$ = $('.app-intro').detach()

      nice_work_modal$ = $ '#nice-work'

      nice_work_modal$.trigger('reveal:close')

      @email = @options.customer_email

      # if sign up customer is present, you have "working_customer.at(0)" (Collections.Customers)
      @sign_up_customer = @options.sign_up_customer

      $('html,body').addClass 'eP-dashboard'

      $('.app--page').removeClass 'landing'

      @render()

      if @sign_up_customer

        @customers = @sign_up_customer

        @customers.bind('change', @update_dashboard, @)

        @customers = @customers.save()

      else

        loaded_customer = new eP.App.Models.Customer { id: @email }

        loaded_customer.bind('change', @update_dashboard, @)
       
        # GET for customer data
        @customers = loaded_customer.fetch()

        @customers.done (customer) =>

          @customers = new eP.App.Collections.Customers
          
          @customers.reset [customer]

    update_dashboard: (customer) ->

      @working_customer = customer

      @$el.empty()

      @render()

      pantry_calendar$ = @$el.find('.m.m--pantry-calendar')

      $('.m--pantry-calendar .loader').animate({

        opacity: 0

      }, 250, () ->

        el$ = $ @

        el$.slideUp()

        loaded_pantry = customer.get 'pantry'

        # sort months by date
        @loaded_shipments = _.sortBy loaded_pantry.shipments, (shipment) ->
          shipment.date

        first_date = @loaded_shipments[0].date

        creation_date = moment(first_date)

        month_list = []

        month_panel$ = ''

        # filter the first year
        prepared_shipments = _.reject @loaded_shipments, (shipment, i) ->
          if i >= 12
            shipment

        _.each prepared_shipments, (shipment) =>

          shipment_id = shipment.id
          shipment.day = moment(shipment.date).format('Do')
          shipment.month = moment(shipment.date).format('MMM')
          shipment.year = moment(shipment.date).format('YYYY')
          shipment.full_date = moment(shipment.date).format('dddd, MMMM Do YYYY')
          shipment.timeago = moment(shipment.date).endOf('day').fromNow()
          shipments = new eP.App.Collections.Shipments shipment.items
          shipment.products_list = ''
          p = []
          
          p.push '<ul class="m-body" data-shipment="'+shipment_id+'">'

          # per month
          shipments.each (item) ->

            image = if ( item.get('image') && item.get('image') != 'no image') then item.get('image') else '/static/img/demo/meyers-test-image.png'

            shipment_id = item.get('shipment')

            s = () ->
              o = ''
              if ! parseInt(item.get('quantity'), 10) > 0
                o = 'hide'
              else
                o = 'show'
              o

            p.push '<li class="'+s()+' m--product product--'+item.get('product')+' m--shipment-item shipment-item--'+item.get('id')+'">
                      <div class="m-image">
                        <img class="m-image" src="' + image + '" alt="Drag '+item.get('name')+'" />
                      </div>
                      <div class="m-name">' + item.get('name').replace(/\((.+)\)/, "") + '</div>
                      <ul class="m-control">
                        <li>
                          <div class="m m--control">
                            <div class="m-title">
                              <label for="shipmentitem-'+item.get('id')+'">
                                QTY
                              </label>
                            </div>
                            <div class="m-body">
                              <input title="Add or remove items of this product" class="quantity" readonly="readonly" id="shipmentitem-'+item.get('id')+'" data-product="'+item.get('product')+'" data-shipment="'+item.get('shipment')+'" data-shipmentitem="'+item.get('id')+'" value="'+item.get('quantity')+'" />
                              <span class="quantity-add button" title="Add One"></span>
                              <span class="quantity-remove button" title="Remove One"></label></span>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>'

          p.push '</ul>'

          shipment.products_list = p.join ''

          month_list.push '<tr class="assignment-panel">
            <td class="month-label">
              <div data-hint="' + shipment.full_date + ', ' + shipment.timeago + '" class="day hint--top hint--info hint--rounded">' + shipment.day + '</div>
              <div class="month">' + shipment.month + '</div>
              <div class="year">' + shipment.year + '</div>
            </td>
            <td class="products-list">
              <div class="m m--products">' + shipment.products_list + '</div>
            </td>
            <td class="price-comparison">
              <dl>
                <dt class="price-term">85</dt>
                <dd>Retail</dd>
                <!--
                  <dt>0</dt>
                  <dd>Online</dd>
                -->
                <dt class="price-term">' + shipment.price + '</dt>
                <dd>ePantry</dd>
              </dl>
            </td></tr>'

          month_panel$ = $ month_list.join ''

        $('.m-calendar').append month_panel$
      )

    render: () ->

      template = _.template $("#page--pantry").html()

      @$el.html template @customer_data

    events: () ->
      "dropon .assignment-panel": "month_dropon"
      "dropend .assignment-panel": "month_dropend"
      "dropout .assignment-panel": "month_dropout"
      "dropover .assignment-panel": "month_dropover"
      "draginit .m--shipment-item": "product_drag"
      "dragmove .m--shipment-item": "product_move"
      "dragend .m--shipment-item": "product_dragend"
      "change .m--shipment-item input": "update_product"
      "click .m--shipment-item": "shipment_item_click"
      "click .m--shipment-item .quantity-add": "update_shipment"
      "click .m--shipment-item .quantity-remove": "update_shipment"
      "click .reveal-months": "reveal_months"
      "click .reveal-checkout": "reveal_checkout"

    shipment_item_click: (e) ->

      el$ = $ @

      el$.addClass 'drag'

    reveal_months: (e) ->

      e.preventDefault()

      _e e

    reveal_checkout: (e) ->

      e.preventDefault()

      if ! @payment_view

        @payment_view = new eP.App.Views.PaymentView({
            el: $ '.app-modals'
            customer: @working_customer
        })

      else

        @payment_view.initialize()

    update_shipment: (e) ->

      # build object
      
      el$ = $ e.target
      control$ = el$.closest '.m--control'
      i$ = control$.find 'input'
      product = i$.attr 'data-product'
      shipment = i$.attr 'data-shipment'
      shipmentitem = i$.attr 'data-shipmentitem'

      v = i$.val()

      if parseInt v, 10 > 0 && parseInt v, 10 < 10

        if el$.hasClass 'quantity-add'
          ++v

        if el$.hasClass 'quantity-remove'
          --v

        shipment_item = new eP.App.Collections.ShipmentItems

        shipment_item.add [{
          id: shipmentitem
        }]

        shipment_item.at(0).url = '/api/shipmentitem/update/' + shipmentitem + '/'

        shipment_update = shipment_item.at(0).save({
          quantity: v
        }, {
          patch: true
          success: (model, response, options) ->
            i$.val(model.get 'quantity')
        })

      else

        error_view = new eP.App.MicroViews.ErrorView {
          content: msg
        }

    month_dropout: (e, drop, drag) ->

      drop.element.removeClass 'over'

    month_dropend: (e, drop, drag) ->

      drop.element.removeClass 'over'

      drag.element.remove()

    month_dropon: (e, drop, drag) ->

      # drag
      drag$ = drag.element
      container$ = drag$.closest('.m-body')
      drag$.removeClass 'drag'
      control$ = drag$.find '.m--control'
      i$ = control$.find 'input'
      product = i$.attr 'data-product'
      shipment = i$.attr 'data-shipment'
      shipment_item = i$.attr 'data-shipmentitem'

      # drop
      drop$ = drop.element
      drop_id = drop$.find('.m-body').attr 'data-shipment'

      # shipment
      shipment_items = new eP.App.Collections.ShipmentItems {
        id: shipment_item
        product: product
        quantity: i$.val()
      }


      if drop$.find('.product--' + product).hasClass('show')

        # we've found a hidden dupe

        shipment_items.at(0).url = '/api/shipmentitem/update/' + shipment_item + '/'

        shipment_update = shipment_items.at(0).save({
          shipment: drop_id
        }, {
          #success: (model, response, options) ->
            #drop$.find('.m--products > ul.m-body').append drag.element[0].outerHTML

          error: (model, xhr, options) ->
            msg = xhr.responseText

            error_view = new eP.App.MicroViews.ErrorView {
              content: msg
            }

            container$.append drag$[0].outerHTML
        })

      else if drop$.find('.hide.product--' + product).length

        shipment_items.at(0).url = '/api/shipmentitem/update/' + drop$.find('input[data-product=' + product + ']').attr('data-shipmentitem') + '/'

        shipment_update = shipment_items.at(0).save({
          quantity: i$.val()
        }, {
          patch: true
        })

        drag$.after drag$[0].outerHTML

        drop$.find('.m--products > ul.m-body').append drag$[0].outerHTML

    month_dropover: (e, drop, drag) ->

      drop.element.addClass 'over'

    product_dragend: (e, drag) ->

      drag.element.removeClass 'drag'

    product_move: (e, drag) ->

      drag.element.addClass 'drag'

    product_drag: (e, drag) ->

      drag.element.addClass 'drag'

      drag.ghost()

    update_product: (e) ->

      target$ = $ e.target

      @pantries.at(0).save()
 

  # Brand Orbital View

  class eP.App.Views.BrandOrbitalView extends Backbone.View

    initialize: () ->

      @pantries = @options.pantries
      @brands = @options.brands
      @products = @options.products
      @counter = @options.counter
      @removal_type = ''

      recent_type = $('body').data('recent_type')

      if @counter == undefined

        @counter = 0

      else

        @counter++

      $('body').data('counter', @counter)

      if @counter == 0

        @removal_type = 'essential-' + recent_type

      else

        @removal_type = 'specialty-' + recent_type

      @page_title = switch
        when (recent_type[0] == "soap" && @counter == 0) then "Soap &amp; Cleaners - Essentials"
        when (recent_type[0] == "soap" && @counter != 0) then "Soap &amp; Cleaners - Specialty"
        when (recent_type[0] == "paper" && @counter == 0) then "Paper - Essentials"
        when (recent_type[0] == "paper" && @counter != 0) then "Paper - Specialty"
        else ''

      brand_orbital$ = $('.app-content .app--page .brand-orbital')

      brand_orbital$.hide()

      landing_welcome$ = $('.app--page.landing .page').detach()

      app_intro$ = $('.app-intro').detach()

      $('.app-history').append landing_welcome$

      $('.app-history').append app_intro$

      brand_picks$ = $('.app-content .brand-picks').detach()

      $('.app-history').append brand_picks$

      previous_components$ = $('.faq,.discovery-bulk,.discovery-prices').detach()

      @render()

      if @options.initial

        discovery_faq_view = new eP.App.ModalViews.FAQView({ el: $(".app-modals") })

      window.location.hash = '#' + @removal_type

      if @counter == 0 && @options.initial

        product_removal = new eP.App.ModalViews.PreBrandOrbitalView({
            el: $(".app-modals")
        })

    render: () ->

      template = _.template(
        $("#page--brand-orbital").html()
      )

      current_pantry = @pantries.at(0)

      if @counter >= current_pantry.get('temp_products').length
        @counter = 0

      data =
        products: current_pantry.get('temp_products')[@counter]
        removal_type: @removal_type
        page_title: @page_title

      if data
        @$el.prepend template data

    events:
      'click .brand-orbital .remove': 'remove_product'
      'click .brand-orbital .m--product .m-title': 'reveal_product'
      'click .brand-orbital .m--navigation .next': 'save_pantry'
      'click .brand-orbital .m--navigation .back': 'position_previous'

    position_previous: (e) ->

      e.preventDefault()

      recent_type = $('body').data('recent_type')

      answered$ = $('.brand-phase-'+recent_type+'.answered')

      if answered$.length

        window.history.go -(1+answered$.length)

      else
        
        window.location.hash = '#brand-'+recent_type

    reveal_product: (e) ->
 
      e.preventDefault()

      el$ = $ e.target

      selected$ = el$.closest('.m--product').index()

      current_pantry = @pantries.at(0)

      temp_products = current_pantry.get 'temp_products'

      counter = $('body').data 'counter'

      current_products = temp_products[counter]

      product_view = new eP.App.ModalViews.ProductView({
          el: $(".app-modals")
        , data: current_products.attributes.products[selected$]
      })

    remove_product: (e) ->

      e.preventDefault()

      el$ = $ e.target

      _e el$

      container$ = el$.closest('.m--product')

      control$ = el$.closest('.m-control')

      control$.toggleClass 'show'

      container$.toggleClass('exclude')

      excluded_products = @pantries.at(0).get 'excluded_products'

      picked_exclude = container$.find('.m-title a').attr('data-id')

      recent_type = $('body').data('recent_type')

      exclude =
        id: parseInt picked_exclude, 10

      excluded_products = @pantries.at(0).get 'excluded_products'

      if ! excluded_products

        excluded_products = []

      if ! _.where(excluded_products, exclude).length

        excluded_products.push exclude

      else

        excluded_products = _.reject excluded_products, (excluded_product) ->
          if excluded_product.id == exclude.id
            excluded_product

      @pantries.at(0).set 'excluded_products', excluded_products

    save_pantry: (e) ->

      e.preventDefault()
      
      el$ = $ e.target
      
      @brand = $('body').data 'recent_brand'

      counter = $('body').data 'counter'

      products = @pantries.at(0).get 'temp_products'

      products.clean()

      last_brand = _.last @brands.models

      recent_type = $('body').data 'recent_type'

      id$ = $('div.brand-phase-pref[id*="'+$('body').data('recent_brand')+'"]')
        .filter(':not([id*="info"])')
        .filter(':not(.deferred)')

      format_preference_modal$ = id$.filter('[id*="format"]').filter(':not(.deferred)')

      @undelegateEvents()

      id = id$
            .eq(0)
            .attr 'id'

      if format_preference_modal$.length

        format_preference_modal$.addClass('deferred')

        id = format_preference_modal$.attr 'id'

      if counter >= (products.length-1)

        if last_brand.attributes.type == recent_type[0]

          branding_complete_view = new eP.App.ModalViews.BrandingCompleteView({
              el: $(".app-modals")
            , pantries: @pantries
          })

          @delegateEvents({
            'click .brand-orbital .m--navigation .next': 'save_pantry'
          })

        else

          $('body').data 'current_orbital_view', @
         
          window.location.hash = '#' + id

          if $('#' + id).length <= 1 && ! $('#' + id).hasClass('open')

            if $('#' + id).hasClass('reveal-modal')

              $('#' + id).reveal({
                closeOnBackgroundClick: false
                animation: 'fade'
              })

            else

              # flow flow
              multiple_preference_view = new eP.App.AdHocViews.PreferenceWizardView({
                  el: $('.app-content')
                  pantries: @pantries
              })

          id$
            .last()
            .addClass 'last'

      else

        # should check model for deselected product by ID
        if format_preference_modal$.length && ! $('.m--product').has('.m-title:contains("Gel")').hasClass('exclude')

          id = format_preference_modal$.attr 'id'

          window.location.hash = '#' + id

          @delegateEvents @events

        else

          brand_orbital$ = $('.app-content .app--page .brand-orbital')

          brand_orbital$.detach()

          @brand_orbital_view = new eP.App.Views.BrandOrbitalView({
              el: $(".app-content .app--page")
            , pantries: @pantries
            , products: products
            , counter: counter
            , brands: @brands
          })

          $('body').data 'last_orbital_view', @


  class eP.App.MicroViews.ErrorView extends Backbone.View

    el: $('.app--page')

    close: () ->

      @undelegateEvents()

      last_modal$ = $('.error-modal').detach()

    initialize: () ->

      @content = @options.content

      @render()

      @error_modal$ = $('.error-modal')

      @reveal_defaults =
        closeOnBackgroundClick: false
        animation: 'fade'
        closed: () =>

          @close()

      @error_modal$.reveal @reveal_defaults

    render: () ->

      template = _.template $("#app-error").html()

      msg = $.parseJSON @content

      data =
        content: msg.__all__

      # bring to fore, think of Readers
      @$el.find('.m--pantry').prepend template data

    events:

      "click .error-modal .button": "close_modal"

    close_modal: (e) ->

      @error_modal$.filter('.open').trigger 'reveal:close'


  class eP.App.MicroViews.SignUpPaginationView extends Backbone.View

    initialize: () ->

      @maximum_pages = @options.maximum

      @position = @options.position

      @type = @options.subset

      @render()

      prev$ = @$el.find('.m--pagination .prev')

      html = []

      for page in [1..@maximum_pages]

        html.push '<li class="item"><a href="">' + page + '</a></li>'

      prev$.after html.join ''

    events:
      "click .m--pagination .prev": "previous"
      "click .m--pagination .next": "next"

    render: () ->

      template = _.template $("#app-signup-paginator").html()

      @$el.find('#app-signup-paginator').after template

    previous: () ->

      console.log @

    next: () ->

      console.log @


  class eP.App.MicroViews.ProgressBreadcrumbView extends Backbone.View

    initialize: () ->

      position = @options.position

      weight = @options.weight

      $('.m--progress').delay(200).fadeIn()

      $('.app-login').addClass 'hide'
      $('.m--social').addClass 'pending'

      if ! $('.m--progress').length

        @render()
 
      _breadcrumb$ = []

      for p in [1..parseInt(position, 10)]

        _breadcrumb$.push '<li class="question-'+p+' slice">.</li>'

      $('.m--progress ul').html _breadcrumb$.join ''

      @sign_up_pagination_view = new eP.App.MicroViews.SignUpPaginationView({
          el: $('.app-header')
          subset: _breadcrumb$.join ''
      })

      $('.m--progress .percentage').text (15+Math.ceil($('.m--progress li').length * weight))+'%'

    render: () ->

      template = _.template(
        $("#app-progress-breadcrumb").html()
      )

      @$el.find('#app-progress-breadcrumb').after( template )

    progress: (type, weight) ->

      $('.m--progress ul .brand-'+type).remove()

      $('.m--progress ul').append $('<li class="brand-'+type+' slice">.</li>')

      $('.m--progress span').text (15+$('.m--progress li').length * weight)+'%'


eP.App =

  Models: {}

  AdHocViews: {}
  ModalViews: {}
  MicroViews: {}
  Views: {}

  Routers: {}

  Collections: {}

  init: () ->

    eP.Bones()

    new eP.App.Routers.BaseWorkspace()

    Backbone.history.start()


oldSync = Backbone.sync

Backbone.sync = (method, model, options) ->

  options.beforeSend = (xhr) ->

    meta_token$ = $ "meta[name='x-csrf-token']"

    crsf_token = meta_token$.attr 'content'

    xhr.setRequestHeader 'X-CSRFToken', crsf_token
  oldSync(method, model, options)

$(() ->
  eP.App.init()
)
