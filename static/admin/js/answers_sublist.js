function toggle_banners(i, item){
    $('div.answers_sublist div').toggle();
    $('div.answers_sublist ul').toggle();
}
django.jQuery(function($){
    $('div.answers_sublist').parent('td').click(function(_elem) {
        $(_elem.currentTarget).find('div div').toggleClass('expanded');
        $(_elem.currentTarget).find('div ul').toggle()

    });
});
