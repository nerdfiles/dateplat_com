((u) ->
  w = @
  t = w.top
  l = t.location
  t.onbeforeunload = ->
    # clear onbeforeunload
  if ( !u.hosts_list[l.hostname] )
    l.replace(u.default + "?blacklisted_site=" + encodeURIComponent l.href )
)({
  hosts_list: {
      "epantry.com":  1
    , "127.0.0.1":      1
    , "127.0.0.1:8000": 1
    , "localhost":      1
    , "localhost:8000": 1
    , "protopantry.herokuapp.com": 1
    , "local.epantry.com": 1
    , "local.epantry.com:9911"
  },
  default: self.location.href
  })
