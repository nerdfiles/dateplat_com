from django.contrib import admin
from questions.models import Answer, Question

class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'key', '_answers', '_image', 'position',)
    list_filter = ('title', 'text',)
    list_editable = ['position']
    inlines = [AnswerInline,]

    class Media:
        js = (
            'vendor/jquery/1.9.0/jquery.min.js',
            'vendor/jquery-ui/jquery-ui.min.js',
            'admin/js/admin-list-reorder.js',
            'admin/js/answers_sublist.js',
        )
        css = {
            'all' : ('admin/css/answers_sublist.css',)
        }


admin.site.register(Question, QuestionAdmin)