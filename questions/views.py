from rest_framework import generics
from questions.models import Question
from questions.serializers import QuestionSerializer


class QuestionList(generics.ListAPIView):
    model = Question
    serializer_class = QuestionSerializer