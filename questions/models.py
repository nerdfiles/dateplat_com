from django.db import models
from django.template.loader import render_to_string


QUESTION_KEYS = (('people_count', 'People count'), ('babies', 'Babies'), ('cook_much', 'Cook much'), ('age', 'Age'),
                 ('wash_hands', 'Wash hands'), ('clean_home', 'Clean home'), ('bathrooms_count', 'Bathrooms count'),)

class Question(models.Model):
    position = models.IntegerField()
    title = models.CharField(max_length=255, null=True, blank=True,)
    image = models.ImageField(upload_to='images', null=True, blank=True,)
    text = models.CharField(max_length=255, null=True, blank=True,)
    key = models.CharField(choices=QUESTION_KEYS, max_length=32, unique=True)

    def _answers(self):
        answers = self.answers.all()
        return render_to_string('admin/question_list/answer_sub_list.html', {'answers':answers})
    _answers.allow_tags = True

    def _image(self):
        return '<img style="max-width:30px; max-height:30px;" src="%s" />' % self.image
    _image.allow_tags = True

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Question, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.title)

    class Meta:
        ordering = ('position',)


class Answer(models.Model):
    text = models.CharField(max_length=255, null=True, blank=True,)
    question = models.ForeignKey(Question, related_name='answers')
    value = models.SmallIntegerField(default=1, blank=True)

    def __unicode__(self):
        return unicode(self.text)

