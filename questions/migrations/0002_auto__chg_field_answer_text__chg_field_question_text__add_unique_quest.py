# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Answer.text'
        db.alter_column(u'questions_answer', 'text', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Question.text'
        db.alter_column(u'questions_question', 'text', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))
        # Adding unique constraint on 'Question', fields ['key']
        db.create_unique(u'questions_question', ['key'])


    def backwards(self, orm):
        # Removing unique constraint on 'Question', fields ['key']
        db.delete_unique(u'questions_question', ['key'])


        # Changing field 'Answer.text'
        db.alter_column(u'questions_answer', 'text', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Changing field 'Question.text'
        db.alter_column(u'questions_question', 'text', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        u'questions.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {'default': '1', 'blank': 'True'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['questions']