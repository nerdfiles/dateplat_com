.. ePantry documentation master file, created by
   sphinx-quickstart on Mon Feb  4 04:28:53 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for ePantry, pypantry
===================================

Welcome
------------------

..  toctree::
    :maxdepth: 2
    :numbered:

    needful

Project files
------------------

..  toctree::
    :maxdepth: 2
    :numbered:

    wireframes
    workflows

Search
------------------

Try the :ref:`search`.
