ePantry Wireframes
====================

..  toctree::
    :maxdepth: 2

The Goal
-----------------------

After Introduction (the first 7 screens plus the "Branded Products" screens), the model 
should be at a roughly completed state. 

The JSON would roughly look like (in some cases you'll see CoffeeScript syntax):

::

    tentative_pantry =

      # essential metadata

      name: 'tenative_pantry'
      canonical_image: null
      canonical_description: null

      # introduction data

      personnel_magnitude: "1"
      personnel_magnitude_with_babies: F
      bathroom_commitment: "1"
      age_group: "2"
      washer_repeater: null
      cleaning_style: null
      cooking_habits: null

      ## branded products data

      phases: [
        ...
      ]

      orbits: [
        ...
      ]

      # adoption data

      full_name: "..."
      e_mail: "..." # sign-in with e-mail or social networking handle
      passphase: "***"

      address_sh: "..."
      address_sh_apt: "..."
      address_sh_city: "..."
      address_sh_state: "..."
      address_sh_postalcode: "..."

      payment_plan: "M" # M|Y
      payment_auto: "Y" # Y|N

      address_bi: "..."
      address_bi_apt: "..."
      address_bi_city: "..."
      address_bi_state: "..."
      address_bi_postalcode: "..."

      address_bi_cname: ""
      address_bi_cnumber: ""
      address_bi_ccode: ""
      address_bi_cexp_date: "" # M|D/M|M/D
      address_bi_cexp_year: "" # YYYY

Overview
````````````````

Adoption follows Introduction.

Ultimately there are (1) "Needs Phases", (2) "Essential Needs Orbits", and (3) "Specialty 
Needs Orbits". (1) may have (2) or (3) or no orbits; the number of (1) depends on the 
availability of products within all brands, or whether there exist "branded products."

Products by Need
````````````````

A "branded product" might be "hand soap" or "dryer sheets". A "need" might be "paper" 
or "soaps." A need (something satisfied or falling within a brand's categorization) 
best makes sense as a category while a branded product makes the most sense as a tag.

Eventually we want to give branded product introductions to our customers for their 
new pantries. We present branded products filtered by need or utility labels, to give 
the customer insight into which brands offer the desired products, given a need.

Such categories may be "soaps," "paper," etc. as "needs," and these labels combined 
together inform the sense behind customer personas. We can anticipate complex 
arrangements of "needs." For instance, there's the persona of the "frat pack" or the 
"soccer mom."

Brand Phases
:::::::::::::::::::::::

At this point, we may update the ``tenative_pantry`` model with "phase data"; basically 
each phase corresponds to a customer need.

The initial question template should contain a list of the phases to include for a 
pantry, based on available branded products. If one branded product exists, then a 
branded product category should exist to present that branded product at this 
opportunity.

The following updates should be made to the ``tentative_pantry`` model:

::

    tentative_pantry =

      ...

      phases: [

          { # phase

              'type': '{{ type|default:"soap" }}'
            , 'brand': '{{ brand }}'
            , 'prefs': [

              # preferences template

                { 
                    'name': '{{ type|default:"refiller" }}'
                  , 'response': '{{ value|default:"0" }}'
                }

              , { 
                    'name': '{{ type|default:"format" }}'
                  , 'response': '{{ value|default:"0" }}'
                }

              # preferences template

              , {
                    'name': '{{ type|default:"scents" }}'
                  , 'response': [
                      'variety'
                    , 'lavendar'
                    , 'linen'
                  ]
                }

              # preferences template

              , { 

                  ... 

                }

              , ...

              ]

          } # end of phase

        , { # phase

              'type': '{{ type|default:"cleaner" }}'
            , 'brand': '{{ brand }}'
            , 'prefs': [
                { ... }
              , { ... }
              , ...
              ]

          } # end of phase

        , { # phase

              'type': '{{ type|default:"paper" }}'
            , 'brand': '{{ brand }}'
            , 'prefs': [
                { ... }
              , { ... }
              , ...
              ]

          } # end of phase

        , { 
              ... 
          }
        
        , ...

      ]

      ...

The ``tentative_pantry`` should be updated at Brand Phase Exit, or before Brand Orbitals are 
instantiated.

Products Exclusion
``````````````````

A "phase" corresponds to a need. The phase the customer interacts with indirectly 
determines "in-brand" relevance, or which products to present in the orbital. We allow for customers to remove any branded products that may have been set for 
subscription. 

Brand Essentials and Specialties as Orbitals
::::::::::::::::::::::::::::::::::::::::::::

At this point, we present all branded products within a given brand and within a 
given need (which follows a phase) as **a single opt-out list**.

The following updates should be made to the ``tentative_pantry`` model:

::

    tentative_pantry =

      ...

      orbits: {
        exclude: [
          'hand-soap',
          'dishwasher-soap',
          ...
        ]
      }

      ...


Introduction, Pantry Creation
-----------------------------

Landing Screen
````````````````````

The Landing Screen loads the first set of questions.

..  image:: _static/1.PNG
    :width: 500
    :alt: Figure 1.

Wizard - Intro
````````````````````

The first wizard creates a tentative pantry, which follows from the basic questions in 
screens 2-7. The current questions are developed in templates which could have dynamic 
data points for future A/B Testing and re-ordering of the "Introduction" to ePantry.

GET
::::::::::::::::::::

Hard-coded or dynamically generated questions. Currently these questions are available 
through Underscore.js templates. Their data-points come from hard-coded JSON, but this 
JSON could be generated by a back-end toolset, like Python or Ruby.

::

    url: '/pantries/(:id|create-new)/'

    {

      questions = [

          { ... }
        , { ... }
        , { ... }

        ...

      ];

    }

Wireframe for Wizard
::::::::::::::::::::

Each question follows this format, with optional supplementary question or call-out. 
For the time being, questions are presented through *modal window dialog boxes*.

..  image:: _static/bones-pantry-question.png
    :width: 700
    :alt: Figure 1. Pantry Question Wireframe.

Steps 1-6
::::::::::::::::::::

..  image:: _static/2.PNG
    :width: 250
    :alt: Figure 2. Question 1 and 2.

..  image:: _static/3.PNG
    :width: 250
    :alt: Figure 3. Question 3.

..  image:: _static/4.PNG
    :width: 250
    :alt: Figure 4. Question 4.

..  image:: _static/5.PNG
    :width: 250
    :alt: Figure 5. Question 5.

..  image:: _static/6.PNG
    :width: 250
    :alt: Figure 6. Question 6.

..  image:: _static/7.PNG
    :width: 250
    :alt: Figure 7. Question 7.

Brand Phases (per Need)
``````````````````````````

Brand Phases are introductions to insight into our products store by brands first, 
like how "startup restaurants" offer entrees first then allow for the inclusion or 
exclusion of other attributes, toward pre-determined experience of value.

We sort by Need and provide metadata, summary and ranking data on branded products, 
with emphasis on brand discovery.

Overview
::::::::::::::::::::

..  image:: _static/phase-workflow.PNG
    :width: 1000
    :alt: Figure Phase Outline.

GET
''''''''''''''''''''

::

    url: '/pantries/:id/{{ need_type }}/'

    {
      needs: '{{ need_type }}',
      url: 'http://epantry.com/{{ need_type }}/',
      brands: [
        { 
          name: '{{ brand_name }}',
          slug: '{{ brand_name_slug }}',
          url: 'http://epantry.com/{{ need_type }}/{{ brand_name }}/',
          canonical_image: '{{ brand_logo }}',
          price_weight: '{{ brand_price_category }}'
        },
        { 
          name: '{{ brand_name }}',
          slug: '{{ brand_name_slug }}',
          url: 'http://epantry.com/{{ need_type }}/{{ brand_name }}/',
          canonical_image: '{{ brand_logo }}',
          price_weight: '{{ brand_price_category }}'
        },
        { 
          name: '{{ branded_name }}',
          slug: '{{ brand_name_slug }}',
          url: 'http://epantry.com/{{ need_type }}/{{ brand_name }}/',
          canonical_image: '{{ brand_logo }}',
          price_weight: '{{ brand_price_category }}'
        }
      ]
    }

Wireframe for Step 7
''''''''''''''''''''

..  image:: _static/branded-product-paths-as-phases.png
    :width: 700
    :alt: Figure BPP.

Step 7
''''''''''''''''''''

..  image:: _static/9.PNG
    :width: 700
    :alt: Figure 9.

GET
::::::::::::::::::::

::

    url: '/pantries/:id/{{ need_type }}/{{ pref_type }}/'

    {
      brand: '{{ brand_name }}',
      canonical_image: '{{ brand_logo }}'
      need_type: '{{ need_type }}',
      preference: '{{ pref_type }}',
      preference_question: '...',
      preference_note: '<strong>Refills:</strong> We'll always start your service...'
      options: [
        { 
          name: '{{ pref.group }}',
          recommended: true,
          option: '{{ pref.option }}' 
        },{ 
          name: '{{ pref.group }}',
          option: '{{ pref.option }}' 
        }
      ]
    }

Wireframe for 7.1
''''''''''''''''''''

..  image:: _static/brand-phases-preferences.png
    :width: 700
    :alt: Figure Brand Preferences.

Step 7.1
''''''''''''''''''''

..  image:: _static/14.PNG
    :width: 700
    :alt: Figure 14.

Step 7.2
''''''''''''''''''''

..  image:: _static/15.PNG
    :width: 700
    :alt: Figure 15.

GET
::::::::::::::::::::

::

    url: '/pantries/:id/{{ need_type }}/{{ pref_type }}/'

    {
      name: 'Preference Name (e.g., "Refills")',
      preference_question: '...',
      preference_note: '<strong>Refills:</strong> We'll always start your service...'
      options: [
        {
          id: '{{ pref.name1_id }}',
          label: '{{ pref.name1_label }}', 
          option: '{{ pref.name1_option }}' 
        },{ 
          id: '{{ pref.name2_id }}',
          label: '{{ pref.name2_label }}', 
          option: '{{ pref.name2_option }}' 
        }
      ]
    }

Wireframe for 8.1
''''''''''''''''''''

..  image:: _static/branded-products-preferences-optional.png
    :width: 700
    :alt: Figure Brand Preferences.

Step 8.1
::::::::::::::::::

..  image:: _static/16.PNG
    :width: 700
    :alt: Figure 16.

Step 8.2
::::::::::::::::::

..  image:: _static/brand-options.PNG
    :width: 700
    :alt: Figure 10.

Step 8.3
::::::::::::::::::

..  image:: _static/17.PNG
    :width: 700
    :alt: Figure 17.

Brand Phase Exit
``````````````````

A Brand Phase Exit may lead to another branded product phase or a branded product 
orbital, for giving the choice to exclude branded products on a need-basis.

..  image:: _static/19.PNG
    :width: 700
    :alt: Figure 19.

Brand Orbital (per Brand)
`````````````````````````

Essential or Specialty Orbitals.

GET (R)
::::::::::::::::::::

::

    url: '/pantries/:id/{{ need_type }}/{{ (essential|specialty) }}/'

    [
      { 
        id: '',
        name: 'product', 
        canonical_image: ''
      },{ 
        id: '',
        name: 'product', 
        canonical_image: ''
      },{ 
        id: '',
        name: 'product', 
        canonical_image: ''
      },
      ...
    ]

GET (R) (Detail)
::::::::::::::::::::

::

    url: '/pantries/:id/{{ product }}

    {
      id: '',
      name: '',
      description: '',
      canonical_image: ''
    }

Wireframe for Step 9
::::::::::::::::::::

..  image:: _static/brand-orbital.png
    :width: 700
    :alt: Figure Brand Orbital.

Intro
::::::::::::::::::

..  image:: _static/10.PNG
    :width: 500
    :alt: Figure 10.

Step 9.1
::::::::::::::::::

..  image:: _static/11.PNG
    :width: 500
    :alt: Figure 11.

Step 9.2
::::::::::::::::::

Customer removes a branded product from their pantry. 

..  image:: _static/12.PNG
    :width: 500
    :alt: Figure 12.

REMOVE (D)
''''''''''''''''''''

::

    url: '/pantries/:id/{{ need_type }}/#remove'

JSON Payload for ``DELETE``:

::

    {
      product: '{{ id }}'
    }

Brand Orbital Exit
`````````````````````````

..  image:: _static/20.PNG
    :width: 500
    :alt: Figure 20.

FAQ
``````````````````

Standard FAQ, presented through modal window.

..  image:: _static/13.PNG
    :width: 500
    :alt: Figure 13.

Schedule
``````````````````

Wireframe for Schedule
::::::::::::::::::::::

..  image:: _static/schedule-brands-by-product-or-need.png
    :width: 700
    :alt: Figure Schedule.

Schedule Intro
::::::::::::::::::

..  image:: _static/21.PNG
    :width: 700
    :alt: Figure 21.

Schedule Pricing
::::::::::::::::::

..  image:: _static/23.PNG
    :width: 700
    :alt: Figure 23.

Schedule / Pantry
::::::::::::::::::

..  image:: _static/22.PNG
    :width: 700
    :alt: Figure 22.

Schedule / First Month's Pantry
:::::::::::::::::::::::::::::::

..  image:: _static/29.PNG
    :width: 700
    :alt: Figure 29.

Adoption, Profile Creation
--------------------------

Changes
``````````````````

Standard contact form. Posts JSON to app.

::

    url: '/pantries/:id/change/'

    {
      full_name: "",
      e_mail: "",
      subject: "Changes to ePantry",
      message: ""
    }

..  image:: _static/34.PNG
    :width: 500
    :alt: Figure 34.

Profile
``````````````````

Standard profile data management form. Posts JSON to app.

::

    url: '/pantries/:id/settings/update'

    {
      old_e_mail: "",
      new_e_mail: "",
      old_passphrase: "",
      new_passphrase: "",

      address_sh: "",
      address_sh_apt: "",
      address_sh_city: "",
      address_sh_state: "", 
      address_sh_postalcode: ""

      address_bi_cname: "",
      address_bi_cnumber: "",
      address_bi_ccode: "",
      address_bi_cexp_date: "", # M|D/M|M/D
      address_bi_cexp_year: "", # YYYY
    }

..  image:: _static/30.PNG
    :width: 500
    :alt: Figure 30.

Adoption
``````````````````

Completing essential profile requirements, presented through modal window.

Adoption Profile
::::::::::::::::::

..  image:: _static/25.PNG
    :width: 500
    :alt: Figure 25.

Adoption Shipping
::::::::::::::::::


..  image:: _static/26.PNG
    :width: 500
    :alt: Figure 26.

Adoption Billing
::::::::::::::::::

..  image:: _static/27.PNG
    :width: 500
    :alt: Figure 27.

Setup Complete
``````````````````

..  image:: _static/28.PNG
    :width: 500
    :alt: Figure 28.
