Where to go
=====================

Some handy links to follow.

Websites
----------------

1. http://epantry.com

Web Repositories
----------------

1. http://github.com/epantry/epantry/tree/dev
2. http://github.com/epantry/epantry/tree/master
3. http://github.com/epantry/epantry-website

Web Apps
----------------

1. http://protopantry.herokuapp.com
2. http://epantrycheckout.herokuapp.com

Tools
----------------

* http://herokuapp.com
* http://github.com
* http://sphinx-doc.org
* http://djangoproject.com
* https://www.djangopackages.com/
* http://coffeescript.org
* http://sass-lang.com
* http://compass-style.org
* http://foundation.zurb.com
* (Possibly) http://johnmacfarlane.net/pandoc/
