#!/bin/bash

pman="manage.py"

echo "[Generating default project test]\n"

#$HOME/.pyenv/versions/dateplat_com/bin/python $pman test -v 2 
#We could prevent commits that do not pass, but for now, we'll just delay and warn.
#We should ignore files that are not *.py.
#But for now, coders should keep tempo and play in their local stages.

echo "[Generating lints for ./$pman]\n"

$HOME/.pyenv/versions/dateplat_com/bin/pylint $pman

