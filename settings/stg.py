# -*- coding: utf-8 -*- #

from settings.prod import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

STRIPE_API_KEY = ""
STRIPE_PUBLIC_KEY = ""
