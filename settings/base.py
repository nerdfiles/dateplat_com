# -*- coding: utf-8 -*- #

import os


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
  ('nerdfiles', 'hello@nerdfiles.net'),
)

MANAGERS = ADMINS

TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

INTERNAL_IPS = ('127.0.0.1',)

import dj_database_url
DATABASES = {
    'default': dj_database_url.config(default='postgres://nerdfiles:t0d4y1sth3d4y!@localhost:5432/dateplat_com')
}

AUTH_PROFILE_MODULE = 'dateplat_com.user'

ROOT_PATH = os.path.dirname(__file__)
_ = lambda s: s

#STRIPE SETTINGS
STRIPE_API_KEY = "sk_test_lKrc91JMrsjXG9kOllVXTYUJ"
STRIPE_PUBLIC_KEY = "pk_test_DclZNMW6kByRrwCjBGKTDMIf"

# MAILGUN SETTINGS
MAIL_GUN_API_URL = 'https://api.mailgun.net/v3'
MAIL_GUN_API_KEY = 'key-9d3816713f8c9c4fc6b9a72a5756ac2b'
MAIL_GUN_DOMAIN_NAME = 'sandbox741062f29b9a49f1aa3c356ac463ac9f.mailgun.org'
MAIL_GUN_MESSAGE_URL = '%s/%s/messages' % (MAIL_GUN_API_URL, MAIL_GUN_DOMAIN_NAME)

THEME = "cernol"
THEME_DIR = os.path.join(ROOT_PATH, "../themes", THEME)

TEMPLATE_DIRS = (
  os.path.join(THEME_DIR, "templates"),
)

API_ROOT = os.path.join(ROOT_PATH, "../", "api")
DOC_ROOT = os.path.join(ROOT_PATH, "../", "docs", "build", "html")

MEDIA_ROOT = os.path.join(ROOT_PATH, "../media")
MEDIA_URL = '/media/'

STATIC_ROOT = '%s/../static/' % (ROOT_PATH, )
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    '%s/../themes/cernol/static/' % (ROOT_PATH, ),
)
STATICFILES_FINDERS = (
  'django.contrib.staticfiles.finders.FileSystemFinder',
  'django.contrib.staticfiles.finders.AppDirectoriesFinder',
  'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_LOADERS = (
  #('django.template.loaders.cached.Loader', (
    'hamlpy.template.loaders.HamlPyFilesystemLoader',
    'hamlpy.template.loaders.HamlPyAppDirectoriesLoader',
  #)),
  'django.template.loaders.filesystem.Loader',
  'django.template.loaders.app_directories.Loader',
  'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
  'django.middleware.common.CommonMiddleware',
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',
  'django.middleware.clickjacking.XFrameOptionsMiddleware',

  'django.middleware.gzip.GZipMiddleware',
  'pipeline.middleware.MinifyHTMLMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
  'django.contrib.auth.context_processors.auth',
  'django.core.context_processors.debug',
  'django.core.context_processors.i18n',
  'django.core.context_processors.media',
  'django.core.context_processors.static',
  'django.core.context_processors.tz',
  'django.contrib.messages.context_processors.messages',
  'sekizai.context_processors.sekizai',
)

ROOT_URLCONF = 'dateplat_com.urls'

# django app init

INSTALLED_APPS = (
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.sites',
  'django.contrib.messages',
  'django.contrib.staticfiles',
  'django.contrib.admin',
  'django.contrib.admindocs',
  'utils',
  'pipeline',
  'sekizai',
  'gunicorn',
  #'south',
  'rest_framework',
  'jsmin',
  'cssmin',
  'questions',
  'product',
  'settings',
  'dateplat_com',
  'schedule',
  'mailing',
)

REST_FRAMEWORK = {
  'FILTER_BACKEND': 'rest_framework.filters.DjangoFilterBackend'
}

#SOUTH_DATABASE_ADAPTERS = {
    #'default': "south.db.postgresql_psycopg2"
#}


# DJANGO PIPELINE

STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.cssmin.CSSMinCompressor'

PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.jsmin.JSMinCompressor'

PIPELINE_COMPILERS = (
  'pipeline.compilers.sass.SASSCompiler',
)

PIPELINE_CSS = {
  'presentation': {
    'source_filenames': (
      'css/ui/stylesheets/theme.css',
      'vendor/nouislider/nouislider.ep.css',
      'vendor/hint.min.css',
    ),
    'output_filename': 'css/ui/theme.css',
  }
}

PIPELINE_JS = {

  'js_init': {
    'source_filenames': (
      #'js/build/lib/frame_buster.js',
      'css/ui/javascripts/vendor/custom.modernizr.js',
    ),
    'output_filename': 'js/build/init.js',
  },

  'js_behavior': {
    'source_filenames': (

      # JS Framework
      'vendor/jquery/dist/jquery.min.js',
      #'https://cdnjs.cloudflare.com/ajax/libs/angular-material-icons/0.6.0/angular-material-icons.min.js',

      # jQuery Dependent
      'css/ui/javascripts/foundation/jquery.foundation.reveal.js',
      'vendor/jquery.waypoints.min.js',
      'vendor/jquery.jquerypp/1.0.1/jquerypp.min.js',

      # jQuery agnostic
      'vendor/nouislider/jquery.nouislider.min.js',
      'vendor/moment.min.js',

      # Template Tools
      'vendor/underscore.js/1.4.4/underscore.min.js',
      'vendor/underscore.string.min.js',

      'vendor/backbone.js/0.9.10/backbone.min.js',

      'js/build/app.js',
    ),
    'output_filename': 'js/build/ui.js',
  },

}

'''
@todo Figure out what to do with these.

/%script{ :src => '{{ STATIC_URL }}vendor/backbone.forms/backbone.forms.min.js' }
/%script{ :src => '{{ STATIC_URL }}vendor/backbone.forms/editors/list.min.js' }


@todo Integrate into templates

/Stripe Integration

%script{ :src => 'https://js.stripe.com/v1/' }
:javascript
  Stripe.setPublishableKey('{{ STRIPE_PUBLIC_KEY }}');
'''

## externalize app customizations

SECRET_KEY = '*s_9@zl*c*@0$x#i!ui@l+10j%mfq1x%)i*_2(&nn6a@b=a-o='

LOGGING = {
  'version': 1,
  'disable_existing_loggers': True,
  'formatters': {
    'standard': {
      'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
    },
    'verbose': {
      'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
    },
    'simple': {
      'format': '%(levelname)s %(message)s'
    },
  },
  'handlers': {
    'file': {
      'level': 'INFO',
      'class': 'logging.handlers.RotatingFileHandler',
      'filename': os.path.join(ROOT_PATH, '../_log', 'requests.log'),
      'maxBytes': 1024*1024*5, # 5MB
      'backupCount': 10,
      'formatter': 'standard'
    },
    'file_userlogins': {              # define and name a handler
      'level': 'DEBUG',
      'class': 'logging.FileHandler', # set the logging class to log to a file
      'formatter': 'verbose',         # define the formatter to associate
      'filename': os.path.join(ROOT_PATH, '../_log', 'userlogins.log') # log file
    },
    'file_usersaves': {               # define and name a second handler
      'level': 'DEBUG',
      'class': 'logging.FileHandler', # set the logging class to log to a file
      'formatter': 'verbose',         # define the formatter to associate
      'filename': os.path.join(ROOT_PATH, '../_log', 'usersaves.log')  # log file
    },
  },
  'loggers': {
    'django.request': {
      'handlers': ['file'],
      'level': 'INFO',
      'propagate': False,
    },
    'logview.userlogins': {            # define a logger - give it a name
      'handlers': ['file_userlogins'], # specify what handler to associate
      'level': 'INFO',                 # specify the logging level
      'propagate': True,
    },
    'logview.usersaves': {             # define another logger
      'handlers': ['file_usersaves'],  # associate a different handler
      'level': 'INFO',                 # specify the logging level
      'propagate': True,
    },
  }
}

# app
WSGI_APPLICATION = 'dateplat_com.wsgi.application'
