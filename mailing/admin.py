from django.contrib import admin
from mailing.models import Template, Message


class TemplateAdmin(admin.ModelAdmin):
    list_display = ('key', 'sender', 'subject',)


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'subject', 'status')
    list_filter = ['status',]


admin.site.register(Template, TemplateAdmin)
admin.site.register(Message, MessageAdmin)
