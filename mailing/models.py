from django.db import models
from django.forms.models import model_to_dict
from django_extensions.db.fields import json
import requests
from settings.base import MAIL_GUN_MESSAGE_URL, MAIL_GUN_API_KEY


TEMPLATE_KEYS = ((0, 'Sign up'), (1, 'Order completed'))
MESSAGE_STATUS_CHOICES = ((0, 'Not sent'), (1, 'Queued'))


class Base(models.Model):
    sender = models.EmailField()
    subject = models.CharField(max_length=512)
    text = models.TextField()
    html = models.TextField()

    class Meta:
        abstract = True


class Template(Base):
    key = models.IntegerField(choices=TEMPLATE_KEYS, unique=True)

    def create_message(self,data, to):
        msg = Message.objects.create(
            to=to,
            data=data,
            sender=self.sender,
            subject=self.subject,
            text=self.text,
            html=self.html,
        )
        return msg


class Message(Base):
    to = models.EmailField()
    cc = models.EmailField(null=True, blank=True)
    bcc = models.EmailField(null=True, blank=True)
    data = json.JSONField()
    status = models.IntegerField(default=0, choices=MESSAGE_STATUS_CHOICES)

    def _prepare_data_dict(self):
        data = model_to_dict(self, fields=['to', 'subject', 'text', 'html'])
        data['from'] = self.sender
        data['recipient-variables'] = str(self.data).replace("'", '"')

        return data

    def send(self):
        data = self._prepare_data_dict()
        resp = requests.post(
            MAIL_GUN_MESSAGE_URL,
            auth=('api', MAIL_GUN_API_KEY),
            data=data
        )
        if resp.status_code == 200:
            self.status = 1
            self.save()
