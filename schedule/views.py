from rest_framework import generics, status
from schedule.models import ShipmentItem, Shipment
from schedule.serializer import ShipmentItemSerializer, ShipmentSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

class ShipmentItemUpdate(generics.UpdateAPIView):
    model = ShipmentItem
    serializer_class = ShipmentItemSerializer

class ShipmentCharge(APIView):
    def post(self, request, format=None):
        
        shipment_id = request.DATA['shipment_id']
        
        shipment = Shipment.objects.get(pk=shipment_id)
        if shipment is None or 'stripeToken' not in request.DATA:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        shipment.charge(request.DATA['stripeToken'])
        serializer = ShipmentSerializer(shipment)
        
        return Response(serializer.data)