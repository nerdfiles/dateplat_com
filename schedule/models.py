# -*- coding: utf-8 -*- #
from annoying.functions import get_object_or_None

from django.db import models
from django.conf import settings
from product.models import Product
from dateplat_com.models import Pantry
import datetime
import stripe

#utils

_ = lambda s: s

SHIPMENT_STATUS_CHOICES = (
        (0, 'Scheduled'),
        (1, 'Shipped'),
        (2, 'Exception'),
    )


class Shipment(models.Model):
    pantry = models.ForeignKey(Pantry, related_name='shipments')
    date = models.DateField(help_text=_('Shipment date'))
    status = models.PositiveSmallIntegerField(choices=SHIPMENT_STATUS_CHOICES, default=0)
    charge_id = models.TextField(blank=True, null=True)
    _product_balance_dict = None

    @property
    def price(self):
        return sum(i['product__price'] * i['quantity'] for i in self.items.values('product__price', 'quantity'))

    @property
    def product_balance_dict(self):
        if not self._product_balance_dict:
            self._product_balance_dict = \
            {item['product__pk'] : item['balance'] for item in self.items.values('product__pk', 'balance')}
        return self._product_balance_dict

    def get_product_balance(self, product):
        shipment_item = get_object_or_None(ShipmentItem, shipment=self, product=product)
        if shipment_item:
            return shipment_item.balance
        return 0

    def create_shipment_item(self, product, need):
        quantity = product.get_total(need)
        balance = quantity * product.volume - need
        shipment_item = ShipmentItem(shipment=self,
                                     product=product,
                                     quantity=quantity,
                                     balance=balance)
        return shipment_item

    def charge(self, stripe_card_token):
        stripe.api_key = settings.STRIPE_API_KEY

        cust = self.pantry.customer
        stripe_key = cust.stripe_token
        if stripe_key is None or stripe_key == "":
            stripe_key = stripe.Customer.create(card=stripe_card_token,
                                   email=self.pantry.customer.email,
                                   description="Created for shipment ID " + str(self.id) + " on " + str(datetime.datetime.now())
                                   ).id
            cust.stripe_token = stripe_key
            cust.save()

        charge_id = stripe.Charge.create(amount=str(int(self.price*100)), #cents
                             customer=stripe_key,
                             currency='usd',
                             description='Charge for shipment ID ' + str(self.id)
                             ).id

        self.charge_id = charge_id
        self.save()
        return 1


class ShipmentItem(models.Model):
    product = models.ForeignKey(Product,)
    shipment = models.ForeignKey(Shipment, related_name='items')
    quantity = models.IntegerField(default=0,)
    balance = models.FloatField(default=0.00,) #Actually it is : SHIPPED subtract NEED

    @property
    def price(self):
        return self.product.price * self.quantity

    @property
    def name(self):
        return self.product.name

    @property
    def image(self):
        return self.product.image

    class Meta:
        unique_together = ('product', 'shipment',)

    def __unicode__(self):
        return '%s - %d' % (self.product, self.quantity)
