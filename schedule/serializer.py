from rest_framework import serializers
from schedule.models import Shipment, ShipmentItem


class ShipmentItemSerializer(serializers.ModelSerializer):
    price = serializers.Field(source='price')
    name = serializers.Field(source='name')
    image = serializers.Field(source='image')
    class Meta:
        model = ShipmentItem
        exclude = ('balance',)


class ShipmentSerializer(serializers.ModelSerializer):
    items = ShipmentItemSerializer(many=True)
    price = serializers.Field(source='price')
    class Meta:
        model = Shipment