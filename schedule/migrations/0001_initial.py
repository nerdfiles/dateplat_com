# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Shipment'
        db.create_table(u'schedule_shipment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pantry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pypantry.Pantry'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'schedule', ['Shipment'])

        # Adding model 'ShipmentItem'
        db.create_table(u'schedule_shipmentitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['product.Product'])),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schedule.Shipment'])),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'schedule', ['ShipmentItem'])

        # Adding model 'Schedule'
        db.create_table(u'schedule_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pantry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pypantry.Pantry'])),
        ))
        db.send_create_signal(u'schedule', ['Schedule'])

        # Adding M2M table for field shipments on 'Schedule'
        db.create_table(u'schedule_schedule_shipments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('schedule', models.ForeignKey(orm[u'schedule.schedule'], null=False)),
            ('shipment', models.ForeignKey(orm[u'schedule.shipment'], null=False))
        ))
        db.create_unique(u'schedule_schedule_shipments', ['schedule_id', 'shipment_id'])


    def backwards(self, orm):
        # Deleting model 'Shipment'
        db.delete_table(u'schedule_shipment')

        # Deleting model 'ShipmentItem'
        db.delete_table(u'schedule_shipmentitem')

        # Deleting model 'Schedule'
        db.delete_table(u'schedule_schedule')

        # Removing M2M table for field shipments on 'Schedule'
        db.delete_table('schedule_schedule_shipments')


    models = {
        u'product.brand': {
            'Meta': {'object_name': 'Brand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '12', 'decimal_places': '2'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.ProductType']"})
        },
        u'product.producttype': {
            'Meta': {'object_name': 'ProductType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'pypantry.customer': {
            'Meta': {'object_name': 'Customer'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'pypantry.pantry': {
            'Meta': {'object_name': 'Pantry'},
            'answers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['questions.Answer']", 'symmetrical': 'False'}),
            'customer': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['pypantry.Customer']", 'unique': 'True'}),
            'excluded_products': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'excluded_product'", 'symmetrical': 'False', 'to': u"orm['product.Product']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.Product']", 'symmetrical': 'False'})
        },
        u'questions.answer': {
            'Meta': {'object_name': 'Answer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'answers'", 'to': u"orm['questions.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {'default': '1', 'blank': 'True'})
        },
        u'questions.question': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Question'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'schedule.schedule': {
            'Meta': {'object_name': 'Schedule'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pantry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pypantry.Pantry']"}),
            'shipments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['schedule.Shipment']", 'symmetrical': 'False'})
        },
        u'schedule.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pantry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pypantry.Pantry']"})
        },
        u'schedule.shipmentitem': {
            'Meta': {'object_name': 'ShipmentItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['schedule.Shipment']"})
        }
    }

    complete_apps = ['schedule']