from django.contrib import admin
from schedule.models import Shipment, ShipmentItem

class ShipmentItemInline(admin.TabularInline):
    model = ShipmentItem
    extra = 0

class ShipmentAdmin(admin.ModelAdmin):
    list_display = ('pantry', 'status', 'price', 'date')
    list_filter = ('status',)
    date_hierarchy = 'date'
    inlines = [ShipmentItemInline,]

admin.site.register(Shipment, ShipmentAdmin)